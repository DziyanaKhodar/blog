﻿using Blog.Data.Contracts;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Blog.App.ViewComponents
{
    public class BlogPostsAmountViewComponent : ViewComponent
    {
        public IUnitOfWork _unitOfWork;
        public BlogPostsAmountViewComponent(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IViewComponentResult> InvokeAsync(long blogId)
        {
            var amount = _unitOfWork
                .BlogRepository
                .GetPostsAmount(blogId);

            return View("_Text.cshtml", amount);
        }
    }
}
