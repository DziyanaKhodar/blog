﻿using Automapper.Contracts;
using Blog.App.Models.Blog;
using Blog.Data.Contracts;
using Blog.Data.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.App.ViewComponents
{
    public class BlogsSelectorViewComponent : ViewComponent
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IModelUpdating<BasicBlogModel, Blogg> _blogModelConverter;
        private readonly IStringLocalizer<BlogsSelectorViewComponent> _stringLocalizer;
        public BlogsSelectorViewComponent(IUnitOfWork unitOfWork, IModelUpdating<BasicBlogModel, Blogg> blogModelConverter, IStringLocalizer<BlogsSelectorViewComponent> stringLocalizer)
        {
            _unitOfWork = unitOfWork;
            _blogModelConverter = blogModelConverter;
            _stringLocalizer = stringLocalizer;
        }

        public async Task<IViewComponentResult> InvokeAsync(long currentUserId)
        {
            var blogs = await _unitOfWork
                .BlogRepository
                .GetBlogsByOwner(currentUserId);

            var result = new List<BasicBlogModel>();
            result.Add(new BasicBlogModel { Id = 0, BlogTitle = _stringLocalizer["None"].Value });
            result.AddRange(blogs.Select(x => _blogModelConverter.Convert(x)));

            return View("_BlogsSelector.cshtml", result);
        }
    }
}

