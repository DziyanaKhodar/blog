﻿using Blog.App.Models.User;
using Blog.Lib.Contracts;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.App.ViewComponents
{
    public class RolesViewComponent : ViewComponent
    {
        private readonly IAccountPermissionsManager _accountPermissionsManager;
        public RolesViewComponent(IAccountPermissionsManager accountPermissionsManager)
        {
            _accountPermissionsManager = accountPermissionsManager;
        }

        public async Task<IViewComponentResult> InvokeAsync(long userid)
        {
            var roles = await _accountPermissionsManager.GetUserRolesAsync(userid);
            var res = roles.Select(x => new Role
            {
                Name = x.role,
                Member = x.member
            })
            .ToList();
            return View("~/Views/User/_RolesSelect.cshtml", new RolesModel { Roles = res });
        }
    }
}
