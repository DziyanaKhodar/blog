﻿
namespace Blog.App.Configuration
{
    public class CustomSettings
    {
        public string ResourcesStoragePath { get; set; }
        public int PageSize { get; set; }
    }
}
