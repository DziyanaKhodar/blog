﻿using Blog.App.Models.Search;
using Blog.Data.Models;

namespace Blog.App.Contracts
{
    public interface ITagPostsSearcher
    {
        TagsPostsSearchResultModel Search(long[] tags, long userid, int page);
    }
}
