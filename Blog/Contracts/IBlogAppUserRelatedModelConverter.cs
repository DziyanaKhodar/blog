﻿using Automapper.Contracts;

namespace Blog.App.Contracts
{
    public interface IBlogAppUserRelatedModelConverter<U,M> : IUserRelatedModelConverter<U,M,long> where U: class, new() where M : class
    {
    }
}
