﻿using Blog.App.Models.Search;
using Blog.Data.Models;

namespace Blog.App.Contracts
{
    public interface IAllPostsSearcher
    {
        AllPostsSearchResultModel Search(BasicSearchModel searchModel, long userid, int page);
    }
}
