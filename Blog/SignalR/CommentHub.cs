﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;

namespace Blog.App.SignalR
{
    public class CommentHub : Hub
    {
        public override Task OnConnectedAsync()
        {
            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception ex)
        {
            return base.OnDisconnectedAsync(ex);
        }

        public void JoinGroup(string postId)
        {
            Groups.AddToGroupAsync(Context.ConnectionId, postId).Wait();
        }
    }
}
