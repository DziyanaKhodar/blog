﻿using Microsoft.AspNetCore.SignalR;


namespace Blog.App.SignalR
{
    public class EditPostHub: Hub
    {
        public string GetConnectionId()
        {
            return Context.ConnectionId;
        }
    }
}
