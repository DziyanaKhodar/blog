﻿using Automapper.Contracts;
using Automapper.ModelsConverters;
using Blog.App.Models.Post;
using Blog.Data.Contracts;
using Blog.Data.Models;
using Blog.Lib.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Blog.App.ModelsConverters.EntitiesConverters.Tags
{
    public class PostEntityConverter : ModelConverter<Post, EditPostModel>
    {
        private readonly IBlogLogic _blogLogic;
        private readonly IUnitOfWork _unitOfWork;
        public PostEntityConverter
        (
            INameConventionsFieldsBinder binder,
            IEnumerable<IPropertyProcessor> propertyProcessors,
            IServiceProvider serviceProvider,
            IBlogLogic blogLogic,
            IUnitOfWork unitOfWork
        ) 
        : base(binder, propertyProcessors, serviceProvider)
        {
            _blogLogic = blogLogic;
            _unitOfWork = unitOfWork;
        }

        public override void Update(EditPostModel viewModel, Post entity)
        {
            base.Update(viewModel, entity);
            if (viewModel.Tags != null)
            {
                _blogLogic.AddTagsToPost(entity, viewModel.Tags.Select(x => (x.TagText, x.Id)));
            }

            if(viewModel.Id == 0)
            {
                entity.DatePublished = DateTime.UtcNow;
            }
        }
    }
}
