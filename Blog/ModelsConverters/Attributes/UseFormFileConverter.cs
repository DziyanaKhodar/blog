﻿using Automapper.Contracts;
using Automapper.ModelsConverters.Attributes;
using Microsoft.AspNetCore.Http;


namespace Blog.App.ModelsConverters.Attributes
{
    public class UseFormFileConverter : UseBasicConverterAttribute
    {
        public UseFormFileConverter(): base(typeof(IConverter<IFormFile, string>))
        {

        }
    }
}
