﻿using Automapper.Contracts;
using Blog.App.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text.RegularExpressions;

namespace Blog.App.ModelsConverters.FieldsConverters
{
    public class FormFileConverter : IConverter<IFormFile, string>
    {
        private readonly CustomSettings _settings;
        public FormFileConverter(IOptions<CustomSettings> options)
        {
            _settings = options.Value;
        }

        public string Convert(IFormFile formFile)
        {
            if(formFile == null)
            {
                return string.Empty;
            }
            var filename = Regex.Replace(Guid.NewGuid().ToString(), "-", string.Empty);
            var filePath = Path.Combine(_settings.ResourcesStoragePath, string.Concat(filename, ".png"));
            using (var fs = File.Open(filePath, FileMode.CreateNew))
            {
                using (var ms = new MemoryStream())
                {
                    formFile.CopyTo(ms);
                    var bmp = new Bitmap(ms);
                    bmp.Save(fs,ImageFormat.Png);
                }
                
            }
            return filePath;
        }
    }
}
