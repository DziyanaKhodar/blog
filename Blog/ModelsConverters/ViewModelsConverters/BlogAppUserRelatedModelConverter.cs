﻿using Automapper.Contracts;
using Blog.App.Contracts;
using Blog.Data.Contracts;
using Blog.Data.Models;


namespace Blog.App.ModelsConverters.ViewModelsConverters
{
    public class BlogAppUserRelatedModelConverter<T, U> : IBlogAppUserRelatedModelConverter<T, U> where T : class, new() where U : class
    {
        private readonly IUserRelatedModelUpdating<T, U, User> _innerConverter;
        private readonly IUnitOfWork _unitOfWork;
        public BlogAppUserRelatedModelConverter
        (
            IUserRelatedModelUpdating<T,U,User> innerConverter,
            IUnitOfWork unitOfWork
        )
        {
            _innerConverter = innerConverter;
            _unitOfWork = unitOfWork;
        }

        public T ConvertUserRelated(U source, long userid)
        {
            var user = _unitOfWork.UserRepository.GetById(userid);
            return _innerConverter.ConvertUserRelated(source, user);
        }
    }
}
