﻿using Automapper.Contracts;
using Automapper.ModelsConverters;
using Blog.App.Models.Post;
using Blog.App.Models.Tag;
using Blog.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Blog.App.ModelsConverters.ViewModelsConverters.Posts
{
    public class EditPostModelConverter : ModelConverter<EditPostModel, Post>
    {
        IModelUpdating<TagModel, Tag> _tagConverter;
        public EditPostModelConverter
        (
            INameConventionsFieldsBinder binder,
            IEnumerable<IPropertyProcessor> propertyProcessors,
            IServiceProvider serviceProvider,
            IModelUpdating<TagModel, Tag> tagConverter
        ):base(binder, propertyProcessors, serviceProvider)
        {
            _tagConverter = tagConverter;
        }

        public override void Update(Post entity, EditPostModel model)
        {
            base.Update(entity,model);
            model.Tags = new List<TagModel>(entity.TagPosts.Select(x => x.Tag).Select(x => _tagConverter.Convert(x)));
        }
    }
}
