﻿using Automapper.Contracts;
using Automapper.ModelsConverters;
using Blog.App.Models.Post;
using Blog.App.Models.Tag;
using Blog.App.Models.User;
using Blog.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Blog.App.ModelsConverters.ViewModelsConverters.Posts
{
    public class BasicPostModelWithTagsAndUserConverter : ModelConverter<BasicPostModelWithTagsAndUser, Post>
    {
        private readonly IModelUpdating<TagModel, Tag> _tagConverter;
        private readonly IModelUpdating<BasicUserModel, User> _userConverter;
        public BasicPostModelWithTagsAndUserConverter
        (
            INameConventionsFieldsBinder nameConventionsFieldsBinder,
            IEnumerable<IPropertyProcessor> propertyProcessors,
            IServiceProvider serviceProvider,
            IModelUpdating<TagModel, Tag> tagConverter,
            IModelUpdating<BasicUserModel, User> userConverter

        ) : base(nameConventionsFieldsBinder, propertyProcessors, serviceProvider)
        {
            _tagConverter = tagConverter;
            _userConverter = userConverter;
        }

        public override void Update(Post entity, BasicPostModelWithTagsAndUser model)
        {
            base.Update(entity, model);
            model.Tags = new List<TagModel>(entity.TagPosts.Select(x => x.Tag).Select(x => _tagConverter.Convert(x)));
            model.User = _userConverter.Convert(entity.Blog.Owner);
        }
    }
}
