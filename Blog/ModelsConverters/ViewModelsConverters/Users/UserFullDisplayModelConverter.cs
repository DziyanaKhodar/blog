﻿using Automapper.Contracts;
using Automapper.ModelsConverters;
using Blog.App.Models.User;
using Blog.Data.Contracts;
using Blog.Data.Models;
using Blog.Lib.Contracts;
using System;
using System.Collections.Generic;

namespace Blog.App.ModelsConverters.ViewModelsConverters.Users
{
    public class UserFullDisplayModelConverter : UserRelatedModelConverter<UserFullDisplayModel, User, User>
    {
        private readonly IModelUpdating<BasicUserModel, User> _basicUserModelConverter;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPermissions _permissions;

        public UserFullDisplayModelConverter
        (
            INameConventionsFieldsBinder nameConventionsFieldsBinder,
            IEnumerable<IPropertyProcessor> propertyProcessors,
            IEnumerable<IUserRelatedPropertyProcessor<User>> userRelatedPropertyProcessors,
            IServiceProvider serviceProvider,
            IModelUpdating<BasicUserModel, User> basicUserModelConverter,
            IUnitOfWork unitOfWork,
            IPermissions permissions

        ) : base(nameConventionsFieldsBinder, propertyProcessors, userRelatedPropertyProcessors, serviceProvider)
        {
            _basicUserModelConverter = basicUserModelConverter;
            _unitOfWork = unitOfWork;
            _permissions = permissions;
        }

        public override void UpdateUserRelated(User entity,  UserFullDisplayModel model, User user)
        {
            base.UpdateUserRelated(entity, model, user);
            model.User = _basicUserModelConverter.Convert(entity);
            //var user = _unitOfWork.UserRepository.GetById((long)userid);
            //model.CanBan = _permissions.CanBan(entity, user);
            //model.CanRemoveFromBan = _permissions.CanRemoveFromBan(entity, user);
            //model.CanChangeRole = _permissions.CanChangeRole(entity, user);
        }
    }
}
