﻿using Automapper.Contracts;
using Blog.App.Configuration;
using Blog.App.Contracts;
using Blog.App.Models.Post;
using Blog.App.Models.Search;
using Blog.Data.Contracts;
using Blog.Data.Models;
using Blog.Lib.Contracts;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using X.PagedList;

namespace Blog.App.ModelsConverters.ViewModelsConverters.Search
{
    public class AllPostsSearcher : IAllPostsSearcher
    {
        private readonly int _pageSize;
        private readonly IBlogLogic _blogLogic;
        private readonly IUserRelatedModelUpdating<EditablePostModel, Post, User> _postModelConverter;
        private readonly IUnitOfWork _unitOfWork;
        public AllPostsSearcher
        (
            IOptions<CustomSettings> options,
            IBlogLogic blogLogic,
            IUnitOfWork unitOfWork,
            IUserRelatedModelUpdating<EditablePostModel, Post, User> postModelConverter
        )
        {
            _pageSize = options.Value.PageSize;
            _blogLogic = blogLogic;
            _postModelConverter = postModelConverter;
            _unitOfWork = unitOfWork;
        }

        public AllPostsSearchResultModel Search(BasicSearchModel model, long userid, int page)
        {
            var tagsOrWords = HttpUtility.UrlDecode(model.TagsOrWords);
            var convertedTagsOrWords = ConvertStringToTagsAndWords(tagsOrWords);
            var user = _unitOfWork.UserRepository.GetById(userid);
            long? userId = model.CurrentUserOnly ? userid : (long?)null;

            var posts = _blogLogic.SearchPostsByUserTagsOrWords(model.UserName, convertedTagsOrWords.tags, convertedTagsOrWords.words, model.BlogId, userId);
            var postCollection = posts
               .ToPagedList(page, _pageSize)
               .Select(x => _postModelConverter.ConvertUserRelated(x, user));

            var allPostsSearchResultModel = new AllPostsSearchResultModel
            {
                CurrentUserOnly = model.CurrentUserOnly,
                BlogId = model.BlogId,
                TagsOrWords = model.TagsOrWords,
                UserName = model.UserName,
                PostCollection = postCollection

            };

            return allPostsSearchResultModel;
        }

        private IEnumerable<string> SplitToWords(string str)
        {
            if (string.IsNullOrWhiteSpace(str))
            {
                return Enumerable.Empty<string>();
            }

            return str.Split(new[] { ' ', '\t', '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
        }

        private (string[] tags, string[] words) ConvertStringToTagsAndWords(string tagsOrWords)
        {
            if (string.IsNullOrWhiteSpace(tagsOrWords))
            {
                return (new string[0], new string[0]);
            }

            tagsOrWords = tagsOrWords.Trim();
            var firstTagMatch = Regex.Match(tagsOrWords, "#");
            if (!firstTagMatch.Success)
            {
                return (new string[0], SplitToWords(tagsOrWords).ToArray());
            }

            var firstTagIndex = firstTagMatch.Index;
            List<string> words = new List<string>();
            List<string> tags = new List<string>();
            if (firstTagIndex != 0)
            {
                var firstWords = SplitToWords(tagsOrWords.Substring(0, firstTagIndex));
                words.AddRange(firstWords);
            }

            var parts = tagsOrWords
                .Substring(firstTagIndex, tagsOrWords.Length - firstTagIndex)
                .Split("#", StringSplitOptions.RemoveEmptyEntries);

            foreach (var part in parts)
            {
                var partWords = SplitToWords(part);
                if (!partWords.Any())
                {
                    continue;
                }

                tags.Add(partWords.First());
                words.AddRange(partWords.Skip(1));
            }

            return (tags.Select(x => x.ToLower()).ToArray(), words.Select(x => x.ToLower()).ToArray());
        }
    }
}
