﻿using Automapper.Contracts;
using Blog.App.Configuration;
using Blog.App.Contracts;
using Blog.App.Models.Post;
using Blog.App.Models.Search;
using Blog.App.Models.Tag;
using Blog.Data.Contracts;
using Blog.Data.Models;
using Microsoft.Extensions.Options;
using System.Linq;
using X.PagedList;

namespace Blog.App.ModelsConverters.ViewModelsConverters.Search
{
    public class TagPostsSearcher : ITagPostsSearcher
    {
        private readonly int _pageSize;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserRelatedModelUpdating<EditablePostModel, Post, User> _postModelConverter;
        private readonly IModelUpdating<TagModel, Tag> _tagModelConverter;
        public TagPostsSearcher
        (
            IOptions<CustomSettings> options,
            IUnitOfWork unitOfWork,
            IUserRelatedModelUpdating<EditablePostModel, Post, User> postModelConverter,
            IModelUpdating<TagModel, Tag> tagModelConverter
        )
        {
            _pageSize = options.Value.PageSize;
            _unitOfWork = unitOfWork;
            _postModelConverter = postModelConverter;
            _tagModelConverter = tagModelConverter;
        }

        public TagsPostsSearchResultModel Search(long[] tags, long userid , int page)
        {
            var posts = _unitOfWork.PostRepository.GetPostsByTagsIds(tags);

            var user = _unitOfWork.UserRepository.GetById(userid);
            var postCollection = posts
               .ToPagedList(page, _pageSize);
            var res = postCollection   .Select(x => _postModelConverter.ConvertUserRelated(x, user));

            var searchTags = _unitOfWork.TagRepository.GetTagsBatch(tags);
            var model = new TagsPostsSearchResultModel
            {
                Tags = searchTags.Select(x => _tagModelConverter.Convert(x)).ToList(),
                PostCollection = res
            };

            return model;
        }
    }
}
