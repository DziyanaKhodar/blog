﻿using Automapper.Contracts;
using Automapper.ModelsConverters;
using Blog.App.Models.Blog;
using Blog.App.Models.Post;
using Blog.Data.Contracts;
using Blog.Data.Models;
using System;
using System.Collections.Generic;


namespace Blog.App.ModelsConverters.ViewModelsConverters.Blogs
{
    public class FullDisplayBlogModelConverter : UserRelatedModelConverter<FullDisplayBlogModel, Blogg, User>
    {
        private readonly IUserRelatedModelUpdating<EditablePostModel, Post, User> _postConverter;
        private readonly IUnitOfWork _unitOfWork;
        public FullDisplayBlogModelConverter
        (
            INameConventionsFieldsBinder nameConventionsFieldsBinder,
            IEnumerable<IPropertyProcessor> propertyProcessors,
            IEnumerable<IUserRelatedPropertyProcessor<User>> userRelatedPropertyProcessors,  
            IServiceProvider serviceProvider,
            IUserRelatedModelUpdating<EditablePostModel, Post, User> postConverter,
            IUnitOfWork unitOfWork

        ):base(nameConventionsFieldsBinder, propertyProcessors, userRelatedPropertyProcessors, serviceProvider)
        {
            _postConverter = postConverter;
            _unitOfWork = unitOfWork;
        }

        public override void UpdateUserRelated(Blogg entity, FullDisplayBlogModel model, User user)
        {
            base.UpdateUserRelated(entity, model, user);
            var latestPost = _unitOfWork.PostRepository.GetLatestBlogPost(entity.Id);
            if (latestPost != null)
            {
                model.LatestPost = _postConverter.ConvertUserRelated(latestPost, user);
            }
        }
    }
}
