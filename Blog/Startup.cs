﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Blog.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using Blog.App.Configuration;
using Blog.Data.Contracts;
using Blog.App.Contracts;
using Blog.App.ModelsConverters.ViewModelsConverters;
using Blog.Data.Models;
using Blog.App.Models.User;
using Blog.App.Models.Post;
using Blog.App.Models.Blog;
using Blog.Lib.Contracts;
using Blog.Lib;
using Blog.App.ModelsConverters;
using Blog.App.ModelsConverters.ViewModelsConverters.Blogs;
using Blog.App.ModelsConverters.ViewModelsConverters.Posts;
using Blog.App.ModelsConverters.ViewModelsConverters.Users;
using Blog.App.ModelsConverters.ViewModelsConverters.Search;
using Blog.App.ModelsConverters.FieldsConverters;
using Blog.Data.Constants;
using Blog.App.Authorization;
using Blog.App.ModelsConverters.EntitiesConverters.Tags;
using Microsoft.AspNetCore.Mvc.Authorization;
using Blog.Data.Repositories;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc.Razor;
using System.Globalization;
using Blog.App.SignalR;
using AutoMapper;
using Automapper.Contracts;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace Blog
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddSignalR();
            services.AddScoped<IUserClaimsPrincipalFactory<User>, ClaimsPrincipalFactory>();

            services.AddDbContext<BlogDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<User, Blog.Data.Models.Role>(options => { options.User.RequireUniqueEmail = true; })
                .AddRoleManager<RoleManager<Blog.Data.Models.Role>>()
                .AddEntityFrameworkStores<BlogDbContext>()
                .AddDefaultTokenProviders();

            services.ConfigureApplicationCookie(options => {
                options.LoginPath = "/Account/Login";
                options.Cookie.SameSite = SameSiteMode.None;
            });

            services.AddMvc(options => { options.Filters.Add(new AuthorizeFilter()); })
            .AddDataAnnotationsLocalization()
            .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddAuthorization(options =>
                options.AddPolicy(
                    "NotBanned",
                    policy => policy.RequireAssertion(ctx => !ctx.User.HasClaim(ClaimsTypes.Banned, "true"))

            ));

            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IBlogLogic, BlogLogic>();

            services.AddScoped<IBlogRepository, BlogRepository>();
            services.AddScoped<ICommentRepository, CommentRepository>();
            services.AddScoped<IPostRepository, PostRepository>();
            services.AddScoped<ITagRepository, TagRepository>();
            services.AddScoped<IUserRepository, UserRepository>();

            services.AddScoped<IRepository<Blogg>, BlogRepository>();
            services.AddScoped<IRepository<Comment>, CommentRepository>();
            services.AddScoped<IRepository<Post>, PostRepository>();
            services.AddScoped<IRepository<Tag>, TagRepository>();
            services.AddScoped<IRepository<User>, UserRepository>();

            services.AddScoped<IPermissions, Permissions>();

            services.AddHttpsRedirection(options =>
            {
                options.RedirectStatusCode = StatusCodes.Status307TemporaryRedirect;
                options.HttpsPort = 5001;
            });


            services.AddTransient<IAccountPermissionsManager, AccountPermissionsManager>();
            services.AddTransient<IAllPostsSearcher, AllPostsSearcher>();
            services.AddTransient<ITagPostsSearcher, TagPostsSearcher>();
            
            services.AddTransient<IConverter<IFormFile, string>, FormFileConverter>();
            services.AddTransient<IUserRelatedModelUpdating<UserFullDisplayModel, User, User>, UserFullDisplayModelConverter>();
            services.AddTransient<IUserRelatedModelUpdating<FullDisplayBlogModel, Blogg, User>, FullDisplayBlogModelConverter>();
            services.AddTransient<IModelUpdating<BasicPostModelWithTagsAndUser, Post>, BasicPostModelWithTagsAndUserConverter>();
            services.AddTransient<IModelUpdating<Post, EditPostModel>, PostEntityConverter>();
            services.AddTransient<IModelUpdating<EditPostModel, Post>, EditPostModelConverter>();

            services.AddTransient<IUserRelatedModelConverter<UserFullDisplayModel, User, User>, UserFullDisplayModelConverter>();
            services.AddTransient<IUserRelatedModelConverter<FullDisplayBlogModel, Blogg, User>, FullDisplayBlogModelConverter>();
            services.AddTransient<IModelConverter<BasicPostModelWithTagsAndUser, Post>, BasicPostModelWithTagsAndUserConverter>();
            services.AddTransient<IModelConverter<Post, EditPostModel>, PostEntityConverter>();
            services.AddTransient<IModelConverter<EditPostModel, Post>, EditPostModelConverter>();

            services.AddAutoMapper<User, IPermissions>();
            
            services.AddTransient(typeof(IBlogAppUserRelatedModelConverter<,>), typeof(BlogAppUserRelatedModelConverter<,>));


            services.AddLocalization(opts => { opts.ResourcesPath = "Resources"; });
            services.AddMvc()
                .AddViewLocalization(
                    LanguageViewLocationExpanderFormat.Suffix,
                    opts => { opts.ResourcesPath = "Resources"; })
                .AddDataAnnotationsLocalization();

            var configBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("CustomSettings.json");
            var config = configBuilder.Build();
            services.Configure<CustomSettings>(config.GetSection("CustomSettings"));

            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            var supportedCultures = new[]
           {
                //new CultureInfo("ru"),
                new CultureInfo("en")
            };

            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture("en"),
                SupportedCultures = supportedCultures,
                SupportedUICultures = supportedCultures
            });

            app.UseStatusCodePagesWithRedirects("/error/{0}");
            app.UseAuthentication();
            app.UseStaticFiles();

            app.UseSignalR(routes =>
            {
                routes.MapHub<CommentHub>("/commenthub");
                routes.MapHub<EditPostHub>("/editposthub");
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
                routes.MapRoute(
                    "CurrentUserPosts",
                    "{myposts}",
                    defaults: new {controller = "Post", action = "GetCurrentUserPosts" });
                routes.MapRoute(
                    "Post",
                    "posts/{id?}",
                    defaults: new {controller = "Post", action = "Post" }
                    );
                routes.MapRoute(
                    "SearchPosts",
                    "{searchposts}",
                    defaults: new {controller = "Post", action = "SearchPosts" }
                    );
                //routes.MapRoute(
                //    "AddComment",
                //    "post/addcomment/{text?}",
                //    defaults: new {controller = "Post", action = "AddComment"}
                //    );
            });
            //app.UseCookiePolicy();
        }

    }
}
