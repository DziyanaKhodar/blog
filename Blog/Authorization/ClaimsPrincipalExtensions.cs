﻿using System;
using System.Linq;
using System.Security.Claims;


namespace Blog.App.Authorization
{
    public static class ClaimsPrincipalExtensions
    {
        public static long GetId(this ClaimsPrincipal claimsPrincipal)
        {
            var userId = claimsPrincipal.Claims
                .Where(c => c.Type == AuthorizationConstants.IdClaim)
                .Select(c => c.Value).SingleOrDefault();
            return Convert.ToInt64(userId);
        }
    }
}
