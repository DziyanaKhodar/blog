﻿using Blog.App.Authorization;
using Blog.App.Contracts;
using Blog.App.Models.Comment;
using Blog.App.SignalR;
using Blog.Data.Contracts;
using Blog.Data.Models;
using Blog.Lib.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Localization;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Blog.App.Controllers
{
    public class CommentController : Controller
    {

        private readonly IBlogLogic _blogLogic;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IHubContext<CommentHub> _hubContext;
        private readonly IRazorViewEngine _razorViewEngine;
        private readonly IStringLocalizer<CommentController> _stringLocalizer;

        public CommentController(IUnitOfWork unitOfWork, IBlogLogic blogLogic, IHubContext<CommentHub> hubContext, IRazorViewEngine razorViewEngine, IStringLocalizer<CommentController> stringLocalizer)
        {
            _blogLogic = blogLogic;
            _unitOfWork = unitOfWork;
            _hubContext = hubContext;
            _razorViewEngine = razorViewEngine;
            _stringLocalizer = stringLocalizer;
        }

        public async Task<string> RenderToStringAsync(string viewName, object model)
        {
            using (var sw = new StringWriter())
            {
                var viewResult = _razorViewEngine.FindView(ControllerContext, viewName, false);

                if (viewResult.View == null)
                {
                    throw new ArgumentNullException($"{viewName} does not match any available view");
                }

                ViewData.Model = model;
                var viewContext = new ViewContext(
                    ControllerContext,
                    viewResult.View,
                    ViewData,
                    TempData,
                    sw,
                    new HtmlHelperOptions()
                );

                await viewResult.View.RenderAsync(viewContext);
                return sw.ToString();
            }
        }

        [HttpPost]
        [Authorize(Policy = "NotBanned")]
        public async Task<IActionResult> AddComment(string text, long postId, [FromServices] IBlogAppUserRelatedModelConverter<DisplayCommentModel, Comment> modelsConverter)
        {
            var comment = new Comment
            {
                AuthorId = User.GetId(),
                PostId = postId,
                CommentText = text,
                DatePublished = DateTimeOffset.Now

            };
            _unitOfWork.CommentRepository.Add(comment);
            _unitOfWork.Save();
            var created = _unitOfWork.CommentRepository.GetById(comment.Id);

            var model = modelsConverter.ConvertUserRelated(created, User.GetId());
            var html = await RenderToStringAsync("_Comment", model);

            var group = _hubContext.Clients.Group(postId.ToString());
            await group.SendAsync("Send", html);

            return new EmptyResult();
        }

        [HttpPost]
        public async Task<IActionResult> DeleteComment(long commentId)
        {
            var comment = _unitOfWork.CommentRepository.GetById(commentId);
            var success = _blogLogic.DeleteComment(comment, User.GetId());
            if (success)
            {
                await _hubContext.Clients.All.SendAsync("Delete", commentId);
                return new EmptyResult();
            }
            else
            {
                return View("Error", _stringLocalizer["AccessDenied"]);
            }
            
        }

    }
}