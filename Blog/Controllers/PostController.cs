﻿using Automapper.Contracts;
using Blog.App.Authorization;
using Blog.App.Contracts;
using Blog.App.Models.Device;
using Blog.App.Models.Post;
using Blog.App.SignalR;
using Blog.Data.Contracts;
using Blog.Data.Models;
using Blog.Lib.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Blog.App.Controllers
{
    public class PostController : Controller
    {

        private readonly IBlogLogic _blogLogic;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IStringLocalizer<PostController> _stringLocalizer;
        private readonly IHubContext<EditPostHub> _hubContext;

        public PostController(IUnitOfWork unitOfWork, IBlogLogic blogLogic, IStringLocalizer<PostController> stringLocalizer, IHubContext<EditPostHub> hubContext)
        {
            _blogLogic = blogLogic;
            _unitOfWork = unitOfWork;
            _stringLocalizer = stringLocalizer;
            _hubContext = hubContext;
        }

        private void SetRedirectUrl()
        {
            TempData["redirectUrl"] = Request.Headers["Referer"].ToString();
        }

        private string GetRedirectUrl()
        {
            object url = null;
            var doesntExist = TempData == null || !TempData.TryGetValue("redirectUrl", out url);
            if (doesntExist || url == null)
            {
                return "/";
            }
            return url.ToString();
        }

        [HttpGet]
        [Authorize(Policy = "NotBanned")]
        public IActionResult Create(long blogId)
        {
            SetRedirectUrl();
            var displayModel = new EditPostModel();
            displayModel.BlogId = blogId;
            return View("EditPost", displayModel);
        }

        [HttpGet]
        [Authorize(Policy = "NotBanned")]
        public IActionResult Edit(long postid, [FromServices] IModelConverter<EditPostModel, Post> converter)
        {
            SetRedirectUrl();
            var post = _unitOfWork.PostRepository.GetById(postid);
            if (post == null)
            {
                return View("Error", _stringLocalizer["PostNotFound"].Value);
            }

            var displayModel = converter.Convert(post);
            return View("EditPost", displayModel);
        }


        [HttpPost]
        public IActionResult Delete(EditPostModel editPostModel)
        {
            var post = _unitOfWork.PostRepository.GetById(editPostModel.Id);
            if (post == null || _blogLogic.DeletePost(post, User.GetId()))
            {
                return Redirect(GetRedirectUrl());
            }

            return View("Error", _stringLocalizer["AccessDenied"].Value);
        }

        [HttpPost]
        public void Cancel()
        {
            Response.Redirect(GetRedirectUrl());
        }

        [HttpPost]
        [Authorize(Policy = "NotBanned")]
        public IActionResult Save([FromForm]EditPostModel editPostModel, [FromServices] IModelUpdating<Post, EditPostModel> converter)
        {
            if (!ModelState.IsValid)
            {
                return View("EditPost", editPostModel);
            }

            Post post;
            if (!_blogLogic.IsExisting(editPostModel.Id))
            {
                post = new Post();
                converter.Update(editPostModel, post);

                if (!_blogLogic.AddPost(post, User.GetId()))
                {
                    return View("Error", _stringLocalizer["AccessDenied"].Value);
                }
            }
            else
            {
                post = _unitOfWork.PostRepository.GetById(editPostModel.Id);
                converter.Update(editPostModel, post);
                if (!_blogLogic.UpdatePost(post, User.GetId()))
                {
                    return View("Error", _stringLocalizer["AccessDenied"].Value);
                }
            }

            return Redirect(GetRedirectUrl());
        }


        [HttpGet]
        public IActionResult Post(long id, [FromServices] IBlogAppUserRelatedModelConverter<FullDisplayPostModel, Post> converter)
        {
            SetRedirectUrl();
            var post = _unitOfWork.PostRepository.GetById(id);
            if (post == null)
            {
                return View("Error", _stringLocalizer["PostNotFound"].Value);
            }
            var displayModel = converter.ConvertUserRelated(post, User.GetId());
            return View("FullDisplayPost", displayModel);
        }

        [HttpGet]
        public void Back()
        {
            Response.Redirect(GetRedirectUrl());
        }

        [HttpPost]
        public async Task<IActionResult> SendNotification(int positionId, string connectionId)
        {
            var returnCode = HttpStatusCode.InternalServerError;

            var notification = new NotificationMessageModel
            {
                UserEmail = User.Identity.Name,
                ClientId = connectionId,
                PositionId = positionId
            };

            var notificationContent = JsonConvert.SerializeObject(notification);
            notificationContent = notificationContent.Replace("\"", "\\\"");
            var jsonNotification = "{ \"data\" : { \"message\":\"" + notificationContent + "\"}}";

            try
            {
                var outcome = await Notifications.Instance.Hub.SendFcmNativeNotificationAsync(jsonNotification, User.Identity.Name);

                if (outcome != null)
                {
                    if (!((outcome.State == Microsoft.Azure.NotificationHubs.NotificationOutcomeState.Abandoned) ||
                        (outcome.State == Microsoft.Azure.NotificationHubs.NotificationOutcomeState.Unknown)))
                    {
                        returnCode = HttpStatusCode.OK;
                    }
                }
            }
            catch(Exception ex)
            {
                Response.StatusCode = 2;
                Response.Headers.Add("exception", ex.ToString());
                return null;
            }

            return StatusCode((int)returnCode);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Photo([FromBody]Image image)
        {
            var user = _hubContext.Clients.Client(image.CLientId);

            await user.SendAsync("Send", image);

            return StatusCode((int)HttpStatusCode.OK);
        }

    }
}
