﻿using Blog.Lib.Contracts;
using Blog.Data.Contracts;
using Blog.Data.Models;
using Microsoft.AspNetCore.Mvc;
using Blog.App.Models.User;
using Blog.App.Contracts;
using Blog.App.Authorization;
using Microsoft.Extensions.Localization;
using Automapper.Contracts;

namespace Blog.App.Controllers
{
    public class UserController : Controller
    {
        private readonly IBlogLogic _blogLogic;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IStringLocalizer<UserController> _stringLocalizer;

        public UserController(IUnitOfWork unitOfWork, IBlogLogic blogLogic, IStringLocalizer<UserController> stringLocalizer)
        {
            _unitOfWork = unitOfWork;
            _blogLogic = blogLogic;
            _stringLocalizer = stringLocalizer;
        }

        [HttpGet]
        public IActionResult Index(long id, [FromServices] IBlogAppUserRelatedModelConverter<UserFullDisplayModel, User> converter)
        {
            var user = _unitOfWork.UserRepository.GetById(id);
            if (user == null)
            {
                return View("Error", _stringLocalizer["UserNotFound"].Value);
            }
            var model = converter.ConvertUserRelated(user, User.GetId());
            return View("User", model);
        }

        [HttpGet]
        public void MyProfile()
        {
            Response.Redirect(Url.Action("Index", "User", new { id = User.GetId() }));
        }

        [HttpGet]
        public IActionResult EditProfile([FromServices] IModelConverter<EditUserModel, User> converter)
        {
            var user = _unitOfWork.UserRepository.GetById(User.GetId());
            var model = converter.Convert(user);
            return View("EditUserProfile", model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View("EditUserProfile", new EditUserModel());
        }

        [HttpPost]
        public IActionResult EditProfile(EditUserModel model, [FromServices] IModelUpdating<User, EditUserModel> converter)
        {
            if (!ModelState.IsValid)
            {
                return View("EditUserProfile", model);
            }

            User user;
            user = _unitOfWork.UserRepository.GetById(model.Id);
            if (user == null)
            {
                View("Error", _stringLocalizer["UserNotFound"].Value);
            }

            converter.Update(model, user);
            if (!_blogLogic.UpdateUser(user, User.GetId()))
            {
                View("Error", _stringLocalizer["AccessDenied"].Value);
            }

            return RedirectToAction("Index", new { id = user.Id });
        }

        [HttpPost]
        public void Cancel(EditUserModel editUserModel)
        {
            Response.Redirect(Url.Action("Index", new { id = editUserModel.Id }));
        }
    }
}