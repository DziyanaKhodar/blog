﻿using Blog.Data.Models;
using Microsoft.AspNetCore.Mvc;
using X.PagedList;
using Blog.App.Contracts;
using Blog.App.Models.Blog;
using Blog.Data.Contracts;
using Blog.Lib.Contracts;
using Blog.App.Models.Post;
using Blog.App.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;
using Blog.App.Configuration;
using Microsoft.Extensions.Localization;
using Automapper.Contracts;

namespace Blog.App.Controllers
{
    public class BlogController : Controller
    {

        private readonly IBlogLogic _blogLogic;
        private readonly IUnitOfWork _unitOfWork;
        private int _pageSize;
        private readonly IStringLocalizer<BlogController> _stringLocalizer;

        public BlogController(IUnitOfWork unitOfWork, IBlogLogic blogLogic, IOptions<CustomSettings> options, IStringLocalizer<BlogController> stringLocalizer)
        {
            _blogLogic = blogLogic;
            _unitOfWork = unitOfWork;
            _pageSize = options.Value.PageSize;
            _stringLocalizer = stringLocalizer;

        }

        [HttpGet]
        [Authorize(Policy = "NotBanned")]
        public IActionResult Create()
        {
            var createBlogModel = new EditBlogModel();
            return View("EditBlog", createBlogModel);
        }

        [HttpGet]
        [Authorize(Policy = "NotBanned")]
        public IActionResult Edit(long blogId, [FromServices] IModelConverter<EditBlogModel, Blogg> converter)
        {
            var blog = _unitOfWork.BlogRepository.GetById(blogId);
            if (blog == null)
            {
                return View("Error", _stringLocalizer["BlogDoesntExist"].Value);
            }
            var editBlogModel = converter.Convert(blog);
            return View("EditBlog", editBlogModel);
        }

        [HttpPost]
        [Authorize(Policy = "NotBanned")]
        public IActionResult Edit(EditBlogModel editBlogModel, [FromServices] IModelUpdating<Blogg, EditBlogModel> converter)
        {
            if (!ModelState.IsValid)
            {
                return View("EditBlog", editBlogModel);
            }

            Blogg blog;
            blog = _unitOfWork.BlogRepository.GetById(editBlogModel.Id);
            if (blog == null)
            {
                blog = new Blogg();
            }

            converter.Update(editBlogModel, blog);

            if (!_blogLogic.AddOrUpdateBlog(blog, User.GetId()))
            {
                return View("Error", _stringLocalizer["AccessDenied"].Value);
            }

            return RedirectToAction("Index", "User", new { id = User.GetId() });
        }

        [HttpPost]
        public IActionResult Delete(EditBlogModel editBlogModel)
        {
            var blog = _unitOfWork.BlogRepository.GetById(editBlogModel.Id);
            if (blog != null)
            {
                if (!_blogLogic.DeleteBlog(blog, User.GetId()))
                {
                    return View("Error", _stringLocalizer["AccessDenied"].Value);
                }
            }
            return RedirectToAction("Index", "User", new { id = User.GetId() });
        }

        [HttpPost]
        public void Cancel()
        {
            Response.Redirect(Url.Action("Index", "User", new { id = User.GetId() }));
        }

        [HttpGet]
        public IActionResult Blog(long id, [FromServices] IBlogAppUserRelatedModelConverter<FullDisplayBlogModel, Blogg> converter)
        {
            var blog = _unitOfWork.BlogRepository.GetById(id);
            if (blog == null)
            {
                return View("Error", _stringLocalizer["BlogDoesntExist"].Value);
            }

            var displayModel = converter.ConvertUserRelated(blog, User.GetId());
            return View("FullDisplayBlog", displayModel);
        }

        [HttpGet]
        public IActionResult Posts(long blogId, int? page, [FromServices] IBlogAppUserRelatedModelConverter<EditablePostModel, Post> converter)
        {
            var postCollection = GetBlogPostsPagedList(blogId, converter, page);
            ViewBag.Posts = postCollection;
            ViewBag.BlogId = blogId;
            return View("BlogPosts", postCollection);
        }

        private IPagedList<EditablePostModel> GetBlogPostsPagedList(long blogId, IBlogAppUserRelatedModelConverter<EditablePostModel, Post> converter, int? page = null)
        {
            page = page == null || page <= 0 ? 1 : page;
            var posts = _unitOfWork.PostRepository.GetPostsByBlogId(blogId);
            var postCollection = posts
                .ToPagedList((int)page, _pageSize)
                .Select(x => converter.ConvertUserRelated(x, User.GetId()));
            return postCollection;
        }
    }
}