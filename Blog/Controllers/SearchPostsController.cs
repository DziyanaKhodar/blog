﻿using Blog.Lib.Contracts;
using Blog.Data.Contracts;
using Microsoft.AspNetCore.Mvc;
using Blog.App.Models.Search;
using Blog.App.Contracts;
using Microsoft.AspNetCore.Authorization;
using Blog.App.Authorization;

namespace Blog.App.Controllers
{
    [Authorize]
    public class SearchPostsController : Controller
    {

        [HttpGet]
        public IActionResult Search()
        {
            var model = new AllPostsSearchModel();
            model.CurrentUserId = User.GetId();
            return View("SearchPosts", model);
        }

        [HttpGet]
        public IActionResult Tags([FromQuery(Name = "tags")]long[] tags, [FromServices] ITagPostsSearcher searcher, int? page = 1)
        {
            page = page == null || page < 0 ? 1 : page;
            var model = searcher.Search(tags, User.GetId(), (int)page);
            return View("TagSearchResultPage", model);

        }

        [HttpPost]
        public IActionResult Search(AllPostsSearchResultModel model)
        {
            return RedirectToAction("All", model);
        }

        [HttpGet]
        public IActionResult All
        (
            BasicSearchModel model,
            [FromServices] IAllPostsSearcher searcher,
            int? page = 1
        )
        {
            page = page == null || page <= 0 ? 1 : page;
            var resultModel = searcher.Search(model, User.GetId(), (int)page);
            return View("SearchResultPage", resultModel);
        }

        [HttpGet]
        public void Mine()
        {
            Response.Redirect(Url.Action("All", "SearchPosts", new { currentUserOnly = true }));
        }

    }
}
