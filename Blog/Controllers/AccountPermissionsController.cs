﻿using Blog.App.Authorization;
using Blog.App.Models.User;
using Blog.Data.Constants;
using Blog.Lib.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.App.Controllers
{
    public class AccountPermissionsController : Controller
    {
        private readonly IAccountPermissionsManager _accountPermissionsManager;
        private readonly IPermissions _permissions;

        public AccountPermissionsController(IAccountPermissionsManager accountPermissionsManager,  IPermissions permissions)
        {
            _accountPermissionsManager = accountPermissionsManager;
            _permissions = permissions;
        }

        [HttpPost]
        [Route("/ban")]
        [Authorize(Roles = "MODERATOR,ADMIN")]
        public async Task<ActionResult> Ban(long userid)
        {
            var res = await _accountPermissionsManager.Ban(userid, User.GetId());
            return Json(new { success = res });
        }

        [HttpPost]
        [Route("/removeFromBan")]
        [Authorize(Roles = "MODERATOR,ADMIN")]
        public async Task<ActionResult> RemoveFromBan(long userid)
        {
            var res = await _accountPermissionsManager.RemoveFromBan(userid, User.GetId());
            return Json(new { success = res });
        }

        [HttpPost]
        [Route("/changeUserRole")]
        [Authorize(Roles = RolesNames.Admin)]
        public async Task<IActionResult> ChangeRole(long userid, RolesModel rolesModel)
        {
            var roles = rolesModel.Roles.Where(x => x.Member).Select(x => x.Name);
            var res = await _accountPermissionsManager.ChangeRoles(userid, User.GetId(), roles);
            return RedirectToAction("Index", "User", new { id = userid });
        }

    }
}