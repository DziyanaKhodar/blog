﻿using System.Diagnostics;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace Blog.App.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        
        public IActionResult Index()
        {
            return Redirect(Url.Action("Search", "SearchPosts"));
        }

        //[AllowAnonymous]
        //public IActionResult About()
        //{
        //    ViewData["Message"] = "Your application description page.";

        //    return View();
        //}

        //public IActionResult Contact()
        //{
        //    ViewData["Message"] = "Your contact page.";

        //    return View();
        //}
        
        //[Route("/error/{0}")]
        //public IActionResult Error(int statusCode)
        //{
        //    if(statusCode == 1)
        //    {
        //        return View("AccessDenied");
        //    }
        //    return View();
        //}
    }
}
