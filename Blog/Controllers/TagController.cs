﻿using System.Linq;
using Blog.Data.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace Blog.App.Controllers
{
    [ApiController]
    public class ApiController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        public ApiController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [Route("/gettags")]
        public ActionResult GetTagsByFirstLetters(string term)
        {
            var tags = _unitOfWork.TagRepository
                .GetTagsByFirstLetters(term.Trim())
                .Select(t => new { label = t.TagText, value = t.Id })
                .ToArray();
            var res = Json(new { tags });
            return res;
        }
    }
}