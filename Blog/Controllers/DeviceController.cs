﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Blog.App.Models.Device;
using Blog.Data.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.NotificationHubs;

namespace Blog.App.Controllers
{
    [ApiController]
    public class DeviceController : ControllerBase
    {
        private readonly UserManager<User> _userManager;

        public DeviceController(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        private async Task<(bool, long? userId)> VerifyCredentials(string email, string password)
        {
            
            var user = await _userManager.FindByEmailAsync(email);

            if (user == null)
            {
                return (false, null);
            }

            var passwordCorrect = await _userManager.CheckPasswordAsync(user, password);
    
            return (passwordCorrect, user.Id);
        }

        [Route("api/[controller]")]
        [AllowAnonymous]
        [HttpPut]
        public async Task<string> UpdateRegistration(DeviceRegistrationUpdateInfo deviceRegistrationUpdateInfo)
        {
            if(!Request.Headers.ContainsKey("email") || !Request.Headers.ContainsKey("password"))
            {
                Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                return string.Empty;
            }

            var email = Request.Headers["email"];
            var password = Request.Headers["password"];
            var credentialsVerificationResult = await VerifyCredentials(email, password);

            if (!credentialsVerificationResult.Item1)
            {
                Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                return string.Empty;
            }

            var tags = deviceRegistrationUpdateInfo.Accounts;
            var tagsList = deviceRegistrationUpdateInfo.Accounts.ToList();
            tagsList.Add(email);

            var registrationDescription = new FcmRegistrationDescription(deviceRegistrationUpdateInfo.Handle);
            registrationDescription.RegistrationId = deviceRegistrationUpdateInfo.RegistrationId;
            registrationDescription.Tags = tagsList.ToHashSet();

            var hub = Notifications.Instance.Hub;

            await hub.CreateOrUpdateRegistrationAsync(registrationDescription);
            Response.StatusCode = (int)HttpStatusCode.OK;

            return credentialsVerificationResult.Item2.ToString();
        }
    }
}