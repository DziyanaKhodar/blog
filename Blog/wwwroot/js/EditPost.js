﻿var imagesIdsCounter = 0;

let hubUrl = '/editposthub';

var hubConnection;

var hubProxy;

var connectionId;

function setupHubConnection() {
    hubConnection = new signalR.HubConnectionBuilder()
        .withUrl(hubUrl)
        .build();

    hubConnection
        .start()
        .then(function () {
            hubConnection.on("Send", function (data) {
                addImage(data.positionId, "data:image/png;base64, " + data.base64ImageContent);
            });

            hubConnection.invoke("GetConnectionId")
                .then(function (id) {
                    connectionId = id;
                    $('#loadImageRemoteButton').removeAttr('disabled');
            });
        })
        .catch(function (err) {
            return console.error(err.toString());
        });
};

function addImage(positionId, base64ImageContent) {
    var divId = "#" + positionId;
    var img = document.createElement("img");
    $(img).attr("src", base64ImageContent)
        .addClass('postImage')
        .appendTo($(divId));
}

function requestImage() {
    var positionId = addImagePlaceHolder();
    sendNotificationToDevices(positionId);
}

function addImagePlaceHolder() {
    var placeHolder = document.createElement('div');
    var positionId = imagesIdsCounter;

    $(placeHolder).attr('id', positionId)
        .appendTo($("#postTextDiv"));

    imagesIdsCounter += 1;
    return positionId;
}

function sendNotificationToDevices(positionId) {
    $.post("/post/SendNotification?positionId=" + positionId + "&connectionId=" + connectionId);
}

function addImageFromComputer(positionId, file) {
    var reader = new FileReader();

    reader.onloadend = function () {
        addImage(positionId, reader.result);
    };

    reader.readAsDataURL(file);
}

function removeIdsInsidePostTextDivs() {
    $('#postTextDiv > div').removeAttr('id');
}

$(document).ready(function () {

    setupHubConnection();



    $('#fileInput').change(function () {
        var files = document.getElementById('fileInput').files;
        if (files.length > 0) {
            var positionId = addImagePlaceHolder();
            addImageFromComputer(positionId, files[0]);       
        }
    });

 
    $(document).on("keydown", "input", function (e) {
        if (e.which == 13) e.preventDefault();
    });



    $("#postform").submit(function (eventObj) {

        for (var i = 0; i < tags.length; i++) {
            addTagToForm(tags[i][0], tags[i][1], i);
        }

        removeIdsInsidePostTextDivs()
        $('#postTextInput').val($('#postTextDiv').html());
        return true;
    });

    $("#Tags").keypress(function (event) {
        if (event.keyCode == 32) {

            var value = document.getElementById('Tags').value;
            appendTag(tags, value, 0);

        }
    });

    $('#Tags').autocomplete({
        source:
            function (request, response) {
                var param = document.getElementById('Tags').value;
                $.ajax({
                    url: '/gettags',
                    type: 'GET',
                    data: { term: param },
                    dataType: 'json',
                    async: true,
                    success: function (data) {
                        response(
                            $.map(data.tags, function (item) {
                                return { label: item.label, value: item.value };
                            }));
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert(textStatus);
                    },
                    failure: function (response) {
                        alert(response.responseText);
                    }
                });
            },
        minLength: 3,
        focus: function (event, ui) {
            event.preventDefault();
            $('#Tags').val(ui.item.label);
        },
        select: function (event, ui) {
            event.preventDefault();
            appendTag(tags, ui.item.label, ui.item.value);
            return false;
        }

    });
});

function appendTag(tags, value, id) {
     value = value.replace(/\s/g, '');
    if (value) {
        var i = tags.length, foo;
        var alreadyExists = false;
        value = value.toLowerCase();
        while (i--) {
            foo = tags[i];
            if (foo[0] == value) {
                alreadyExists = true;
                break;
            }
        }
        if (alreadyExists) {
            $('#Tags').val('');
            return;
        }
        var divId = "Tag" + value;
        var buttonId = "delete" + value;
        var outerDiv = document.createElement('div');
        $(outerDiv).attr('id', divId).addClass('edit-tag-container');

        var innerDiv = document.createElement('div');
        $(innerDiv).addClass('tag-card').addClass('nohover').append(value).appendTo(outerDiv);

        var button = document.createElement('button');
        $(button).attr('id', buttonId).addClass('delete-tag-button').append('X').click(function () {
            deleteTag(this.id);
        }).appendTo(outerDiv);

        $('#PostTags').append(outerDiv);
        tags.push([value, id]);
        $('#Tags').val('');
    }


};

function deleteTagg(buttonId, tags) {
    var tagText = buttonId.replace(/^delete/, '');
    var i = tags.length;
    while (i--) {
        if (tags[i][0] == tagText) {
            tags.splice(i, 1);
        }
    }
    var tagId = 'Tag' + tagText;
    var tag = document.getElementById(tagId);
    tag.parentNode.removeChild(tag);

};

function addTagToForm(value, id, i) {
    var textName = 'Tags[' + i + '].TagText';
    var idName = 'Tags[' + i + '].Id';

    var nameInputField = document.createElement('input');
    $(nameInputField).attr('type', 'hidden')
        .attr('name', textName)
        .attr('value', value)
        .appendTo($("#FormTags"));

    var idInputField = document.createElement('input');
    $(idInputField).attr('type', 'hidden')
        .attr('name', idName)
        .attr('value', id)
        .appendTo($("#FormTags"));
};

function deleteTag(buttonId) {
    deleteTagg(buttonId, tags);
};