﻿function Ban(id) {
    $.ajax({
        url: '/ban?userid=' + id,
    type: 'POST',
        dataType: 'json',
            async: true,
                success: function (data) {
                    if (data.success) {
                        $('#banbutton').html('<button class="btn btn-default" style="margin:30px" onclick="RemoveFromBan(' + id + ')">Remove from ban</button>')
                    }
                    else {
                        alert('Access denied');
                    }
                },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
    },
    failure: function (response) {
        alert(response.responseText);
    }
});
    };

function RemoveFromBan(id) {
    $.ajax({
        url: "/removeFromBan?userid=" + id,
    type: 'POST',
        dataType: 'json',
            async: true,
                success: function (data) {
                    if (data.success) {
                        $('#banbutton').html('<button class="btn btn-danger" style="margin:30px" onclick="Ban(' + id + ')">Ban</button>')
                    }
                    else {
                        alert('Access denied');
                    }
                },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
    },
    failure: function (response) {
        alert(response.responseText);
    }
});
};