﻿
let hubUrl = '/commenthub';

var hubConnection;

var hubProxy;

function setupHubConnection(postid) { 
    hubConnection = new signalR.HubConnectionBuilder()
        .withUrl(hubUrl)
        .build(); 

    hubConnection
        .start()
        .then(function () {
            hubConnection.on("Send", function (data) {
                $("#Comments").prepend(data);
            });

            hubConnection.on("Delete", function (id) {
                var comment = document.getElementById("Comment" + id);
                comment.parentNode.removeChild(comment);
            });

            hubConnection.invoke("JoinGroup", postid.toString());
        })
        .catch(function (err) {
            return console.error(err.toString());
        });
};

function AddComment(postid) {
    var value = document.getElementById('comment').value;
    $.post("/comment/addcomment?text=" + value + "&postid=" + postid);
};

function DeleteComment(id) {
    $.post("/comment/deletecomment?commentid=" + id);
};

