﻿using Blog.App.Models.Tag;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Blog.App.Models.Post
{
    public class EditPostModel
    {
        public long Id { get; set; }
        public long BlogId { get; set; }

        [Required]
        public string PostTitle { get; set; }

        [Required]
        public string PostText { get; set; }
        public List<TagModel> Tags { get; set; }
    }

}
