﻿using Blog.App.Models.Comment;
using System.Collections.Generic;
using Automapper.ModelsConverters.Attributes;

namespace Blog.App.Models.Post
{
    //use for derived
    public class FullDisplayPostModel : BasicPostModelWithTagsAndUser
    {
        [UseViewModelConverter]
        public IEnumerable<DisplayCommentModel> Comments { get; set; }
        [UsePermissionsCheck]
        public bool CanAddComment { get; set; }
    }
}
