﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.App.Models.Post
{
    public class NotificationMessageModel
    {
        public string ClientId { get; set; }
        public int PositionId { get; set; }
        public string UserEmail { get; set; }
    }
}
