﻿using Blog.App.Models.Tag;
using Blog.App.Models.User;
using System.Collections.Generic;


namespace Blog.App.Models.Post
{
    //!!!!!!tags, use for derived
    public class BasicPostModelWithTagsAndUser : BasicPostModel
    {
        public BasicUserModel User { get; set; }
        public List<TagModel> Tags { get; set; }
    }
}
