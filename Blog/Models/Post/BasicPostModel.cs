﻿using System.ComponentModel.DataAnnotations;


namespace Blog.App.Models.Post
{
    //use basic
    public class BasicPostModel
    {
        public long Id { get; set; }
        public string PostTitle { get; set; }

        [Required]
        public string PostText { get; set; }
        public string DatePublished { get; set; }
        public long BlogId { get; set; }

    }
}
