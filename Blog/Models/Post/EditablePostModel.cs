﻿
using Automapper.ModelsConverters.Attributes;

namespace Blog.App.Models.Post
{
    //!!!! use for derived
    public class EditablePostModel : BasicPostModelWithTagsAndUser
    {
        [UsePermissionsCheck]
        public bool CanEditPost { get; set; }
    }
}
