﻿
namespace Blog.App.Models.Tag
{
    //use basic
    public class TagModel
    {
        public long Id { get; set; }
        public string TagText { get; set; }
        public bool IncludeSearchPosts { get; set; } = true;
    }
}
