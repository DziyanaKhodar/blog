﻿using System.ComponentModel.DataAnnotations;


namespace Blog.App.Models.Account
{
    public class RegisterViewModel
    {
        [Required]
        //[Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        //[Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "PasswordsDontMatch")]
        [DataType(DataType.Password)]
        //[Display(Name = "ConfirmPassword")]
        public string PasswordConfirm { get; set; }
    }
}
