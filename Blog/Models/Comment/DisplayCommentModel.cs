﻿using Automapper.ModelsConverters.Attributes;
using Blog.App.Models.User;
using Blog.App.ModelsConverters.Attributes;

namespace Blog.App.Models.Comment
{
    //use basic
    public class DisplayCommentModel
    {    
        public string CommentText { get; set; }

        public long Id { get; set; }

        public long PostId { get; set; }

        [UseViewModelConverter]
        public BasicUserModel Author { get; set; }

        [UseDefaultDateTimeFormatter]
        public string DatePublished { get; set; }

        [UsePermissionsCheck]
        public bool CanDeleteComment { get; set; }
    }

}
