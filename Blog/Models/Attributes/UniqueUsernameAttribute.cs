﻿using Blog.App.Models.User;
using Blog.Data.Contracts;
using System.ComponentModel.DataAnnotations;

namespace Blog.App.Models.Attributes
{
    internal class UniqueUsernameAttribute : ValidationAttribute
    {

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var unitOfWork = (IUnitOfWork)validationContext.GetService(typeof(IUnitOfWork));
            EditUserModel model = (EditUserModel)validationContext.ObjectInstance;
            var user = unitOfWork.UserRepository.GetByUserName(model.UserName);
            if (user == null || user.Id == model.Id)
            {
                return ValidationResult.Success;
            }

            return new ValidationResult($"Username {model.UserName} is already taken.");
            
        }
    }
}