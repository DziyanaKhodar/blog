﻿
namespace Blog.App.Models.Search
{
    public class AllPostsSearchModel : BasicSearchModel
    {
        public long CurrentUserId { get; set; }
    }
}
