﻿using Blog.App.Models.Post;
using X.PagedList;

namespace Blog.App.Models.Search
{
    public class AllPostsSearchResultModel : BasicSearchModel
    {
        public IPagedList<EditablePostModel> PostCollection { get; set; }
    }
}
