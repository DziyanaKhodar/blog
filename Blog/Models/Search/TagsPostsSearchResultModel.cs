﻿using Blog.App.Models.Post;
using Blog.App.Models.Tag;
using System.Collections.Generic;
using X.PagedList;

namespace Blog.App.Models.Search
{
    public class TagsPostsSearchResultModel
    {
        public IPagedList<EditablePostModel> PostCollection { get; set; }
        public List<TagModel> Tags { get; set; }
    }
}
