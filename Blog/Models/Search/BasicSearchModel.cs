﻿
namespace Blog.App.Models.Search
{
    public class BasicSearchModel
    {
        public bool CurrentUserOnly { get; set; }
        public long? BlogId { get; set; }

        public string UserName { get; set; }
        public string TagsOrWords { get; set; }
    }
}
