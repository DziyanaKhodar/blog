﻿using Blog.App.Contracts;
using Blog.App.ModelsConverters.Attributes;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;


namespace Blog.App.Models.Blog
{
    //use basic converter
    public class EditBlogModel
    {
        public long Id { get; set; }
        [Required]
        public string BlogTitle { get; set; }

        [UseFormFileConverter]
        public IFormFile PhotoPath { get; set; }
    }
}
