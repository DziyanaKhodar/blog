﻿using Blog.App.Models.Post;
using Blog.App.Models.User;
using Automapper.ModelsConverters.Attributes;

namespace Blog.App.Models.Blog
{
    public class FullDisplayBlogModel : BasicBlogModel
    {
        [UseViewModelConverter]
        public BasicUserModel Owner { get; set; }
        public EditablePostModel LatestPost { get; set; }
        public string PhotoPath { get; set; }
    }
}
