﻿using Automapper.ModelsConverters.Attributes;


namespace Blog.App.Models.Blog
{
    //use for derived
    public class ShortDisplayBlogModel : BasicBlogModel
    {
        [UsePermissionsCheck]
        public bool CanEditBlog { get; set; }
    }
}
