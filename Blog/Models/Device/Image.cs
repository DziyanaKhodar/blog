﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.App.Models.Device
{
    public class Image
    {
        public string Base64ImageContent { get; set; }
        public string CLientId { get; set; }
        public int PositionId { get; set; }
    }
}
