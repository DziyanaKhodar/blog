﻿
namespace Blog.App.Models.Device
{
    public class DeviceRegistrationUpdateInfo
    {
        public string Handle { get; set; }
        public string RegistrationId { get; set; }
        public string[] Accounts { get; set; }
    }
}
