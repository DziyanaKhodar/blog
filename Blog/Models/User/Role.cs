﻿
namespace Blog.App.Models.User
{
    public class Role
    {
        public string Name { get; set; }
        public bool Member { get; set; }
    }
}
