﻿using System.Collections.Generic;


namespace Blog.App.Models.User
{
    public class RolesModel
    {
        public List<Role> Roles { get; set; }
    }
}
