﻿using Blog.App.Models.Blog;
using Automapper.ModelsConverters.Attributes;
using System.Collections.Generic;


namespace Blog.App.Models.User
{
    //write your own
    public class UserFullDisplayModel
    {
        public long Id { get; set; }

        public BasicUserModel User { get; set; }

        public string AboutMyself { get; set; }

        public string Email { get; set; }

        [UseViewModelConverter]
        public IEnumerable<ShortDisplayBlogModel> Blogs { get; set; }

        [UsePermissionsCheck]
        public bool CanBan { get; set; }
        
        [UsePermissionsCheck]
        public bool CanRemoveFromBan { get; set; }

        [UsePermissionsCheck]
        public bool CanChangeRole { get; set; }
    }
}
