﻿using Blog.App.Models.Attributes;
using Blog.App.ModelsConverters.Attributes;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Blog.App.Models.User
{
    //use basic
    public class EditUserModel
    {
        public long Id { get; set; }

        [Required]
        [UniqueUsername]
        public string UserName { get; set; }
        public string AboutMyself { get; set; }

        [UseFormFileConverter]
        public IFormFile PhotoPath { get; set; }
    }
}
