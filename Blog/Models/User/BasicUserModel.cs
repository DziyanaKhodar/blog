﻿
namespace Blog.App.Models.User
{
    //use basic converter
    public class BasicUserModel
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string PhotoPath { get; set; }
    }
}
