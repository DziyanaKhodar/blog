﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System.IO;

namespace Blog.App.TagHelpers
{
    [HtmlTargetElement("img", Attributes = "src,default-src")]
    public class ImgTagHelper : TagHelper
    {
        public string Src { get; set; }
        public string DefaultSrc { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            string src = Src;
            if (string.IsNullOrWhiteSpace(Src) || !File.Exists(src))
            {

                src = $"/images/{DefaultSrc}";
                
            }
            output.Attributes.SetAttribute("src", src);
        }


    }
}
