﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.App.TagHelpers
{
    [HtmlTargetElement("p", Attributes ="id")]
    public class RemoveRawHtmlTagHelper: TagHelper
    {
        public override int Order => 0;
        public string Id { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            if(Id != "shortPostText")
            {
                return;
            }

            var content = output.GetChildContentAsync().Result;
         
            var text = content.GetContent();

            var leftBraceSymbol = "&lt;";

            var position = text.IndexOf(leftBraceSymbol);

            if(position == -1)
            {
                context.Items["ShortPostText"] = text;
                return;
            }

            text = text.Substring(0, position);

            context.Items["ShortPostText"] = text;
            output.Content.SetContent(text);
        }
    }
}
