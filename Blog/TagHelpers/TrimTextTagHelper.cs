﻿using Microsoft.AspNetCore.Razor.TagHelpers;


namespace Blog.App.TagHelpers
{
    [HtmlTargetElement("a", Attributes = "max-length")]
    [HtmlTargetElement("p", Attributes = "max-length")]
    public class TrimTextTagHelper : TagHelper
    {
        public int MaxLength { get; set; }
        public override int Order => 1;

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {


            var content = output.GetChildContentAsync().Result;
            string text = string.Empty;
            if (output.Attributes.Contains(new TagHelperAttribute("shortPostText")))
            {
                text = context.Items["ShortPostText"].ToString();                
            }
            else
            {
                text = content.GetContent();
            }
            if(text.Length <= MaxLength)
            {
                return;
            }
            text = string.Concat(text.Substring(0, MaxLength), "...");
            output.Content.SetContent(text);         
        }
    }
}
