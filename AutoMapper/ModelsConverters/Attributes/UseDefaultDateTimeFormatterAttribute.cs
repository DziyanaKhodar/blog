﻿using Automapper.Contracts;


namespace Automapper.ModelsConverters.Attributes
{
    public class UseDefaultDateTimeFormatterAttribute : UseBasicConverterAttribute
    {
        public UseDefaultDateTimeFormatterAttribute():base(typeof(ISimpleDateTimeFormatter))
        {

        }
    }
}
