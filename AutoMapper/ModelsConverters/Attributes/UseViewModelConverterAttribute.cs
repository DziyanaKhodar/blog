﻿using System;


namespace Automapper.ModelsConverters.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class UseViewModelConverterAttribute : Attribute
    {

    }
}
