﻿using System;
using System.Collections.Generic;


namespace Automapper.ModelsConverters.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class UseBasicConverterAttribute : Attribute
    {
        public Type ConverterType { get; }
        public string MethodName { get; set; }
        public IEnumerable<object> AdditionalArguments { get; set; }
        
        public UseBasicConverterAttribute(Type type)
        {
            ConverterType = type;
        }
    }
}
