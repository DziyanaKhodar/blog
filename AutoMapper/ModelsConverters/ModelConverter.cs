﻿using Automapper.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Automapper.ModelsConverters
{
    public class ModelConverter<U, T> : IModelUpdating<U, T> where U : class, new() where T : class
    {
        protected readonly INameConventionsFieldsBinder _binder;
        private readonly IEnumerable<IPropertyProcessor> _propertyProcessors;
        protected readonly IServiceProvider _serviceProvider;
        protected readonly Type _baseClass;

        public ModelConverter
        (
            INameConventionsFieldsBinder binder,
            IEnumerable<IPropertyProcessor> propertyProcessors,
            IServiceProvider serviceProvider
        )
        {
            _binder = binder;
            _propertyProcessors = propertyProcessors;
            _serviceProvider = serviceProvider;
            _baseClass = typeof(U).BaseType;
        }


        protected virtual object GetBaseClassConverter()
        {
            if (_baseClass == typeof(object))
            {
                return null;
            }
            var converterType = typeof(IModelUpdating<,>);
            converterType = converterType.MakeGenericType(_baseClass, typeof(T));
            return _serviceProvider.GetService(converterType);
        }

        protected MethodInfo GetConverterMethod(object converter)
        {
            return converter.GetType().GetMethod("Update", new[] { typeof(T), _baseClass });
        }

        protected void UpdateFromBaseClass(T entity, U initialTarget, MethodInfo method, object converter, params object[] args)
        {
            if (converter == null)
            {
                return;
            }

            var baseClass = typeof(U).BaseType;
            if(method == null)
            {
                throw new Exception($"{converter.GetType()} does not contain needed method");
            }

            var arguments = new object[] { entity, initialTarget }.Concat(args).ToArray();
            method.Invoke(converter, arguments);
        }

        public virtual void Update(T entity, U initialTarget)
        {
        
            var converter = GetBaseClassConverter();
            if (converter != null)
            {
                var method = GetConverterMethod(converter);
                UpdateFromBaseClass(entity, initialTarget, method, converter);
            }

            UpdateInternal(entity, initialTarget);
        }

        protected void UpdateInternal(T entity, U initialTarget)
        {
            _binder.BindFields(entity, initialTarget, _propertyProcessors);
        }

        public virtual U Convert(T entity)
        {
            var target = new U();
            Update(entity, target);
            return target;
        }
    }
}

