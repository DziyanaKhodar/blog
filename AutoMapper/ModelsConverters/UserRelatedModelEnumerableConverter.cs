﻿using Automapper.Contracts;
using System.Collections.Generic;
using System.Linq;


namespace Automapper.ModelsConverters
{
    public class UserRelatedModelEnumerableConverter<T, U, TUser> : IUserRelatedModelEnumerableConverter<T, U, TUser> where T : class, new() where U : class
    {
        private readonly IUserRelatedModelUpdating<T, U, TUser> _viewModelsConverter;
        public UserRelatedModelEnumerableConverter(IUserRelatedModelUpdating<T,U, TUser> viewModelsConverter)
        {
            _viewModelsConverter = viewModelsConverter;
        }
        public IEnumerable<T> ConvertUserRelated(IEnumerable<U> source, TUser user)
        {
            return source.Select(x => _viewModelsConverter.ConvertUserRelated(x, user)).ToList();
        }
    }
}
