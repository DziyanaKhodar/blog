﻿using Automapper.Contracts;
using Automapper.ModelsConverters.Attributes;
using System.Linq;
using System.Reflection;

namespace Automapper.ModelsConverters.FieldsProcessors
{
    public class PermissionsPropertyProcessor<TUser, TPermissions> : IUserRelatedPropertyProcessor<TUser>
    {
        private readonly TPermissions _permissions;

        public PermissionsPropertyProcessor(TPermissions permissions)
        {
            _permissions = permissions;
        }

        public bool ProcessProperty(PropertyInfo targetField, object sourceObject, object targetObject, TUser user)
        {

            if(!targetField.GetCustomAttributes().Any(x => typeof(UsePermissionsCheckAttribute).IsAssignableFrom(x.GetType())))
            {
                return false;
            }

            if (targetField.PropertyType == typeof(bool))
            {
                var permissionMethod = typeof(TPermissions).GetMethod(targetField.Name);
                if(permissionMethod == null)
                {
                    return false;
                }
                var parameters = permissionMethod.GetParameters();
                var returnType = permissionMethod.ReturnType;
                if
                (
                    returnType == typeof(bool) &&
                    parameters.Count() == 2 &&
                    parameters[0].ParameterType == sourceObject.GetType() &&
                    parameters[1].ParameterType == typeof(TUser)                   
                )
                {
                    var newValue = permissionMethod.Invoke(_permissions, new object[] { sourceObject, user });
                    targetField.SetValue(targetObject, newValue);
                    return true;
                }
            }

            return false;
        }
    }
}
