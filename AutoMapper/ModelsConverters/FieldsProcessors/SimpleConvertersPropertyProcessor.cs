﻿using Automapper.Contracts;
using Automapper.ModelsConverters.Attributes;
using System;
using System.Linq;
using System.Reflection;


namespace Automapper.ModelsConverters.FieldsProcessors
{
    public class SimpleConvertersPropertyProcessor : BaseConvertersPropertyProcessor, IPropertyProcessor
    {
        private UseBasicConverterAttribute _attribute;

        public SimpleConvertersPropertyProcessor(IServiceProvider serviceProvider) : base(serviceProvider)
        {

        }

        public bool ProcessProperty(PropertyInfo targetProperty, object sourceObject, object targetObject)
        {
            var sourceProperty = sourceObject.GetType().GetProperty(targetProperty.Name);
            if(sourceProperty == null)
            {
                return false;
            }
            var converterAttributes = targetProperty.GetCustomAttributes()
                .Concat(sourceProperty.GetCustomAttributes())
                .Where(x => typeof(UseBasicConverterAttribute).IsAssignableFrom(x.GetType()))
                .ToArray();

            var amount = converterAttributes.Length;

            if(amount == 0)
            {
                return false;
            }

            if(amount == 2)
            {
                throw new CustomAttributeFormatException("UseSimpleConverterAttribute must be applied either to target or to source field.");
            }

            _attribute = (UseBasicConverterAttribute)converterAttributes.First();
            var methodAndConverter = GetConvertMethod(targetProperty, sourceProperty, sourceObject, targetObject);

            if (methodAndConverter == null)
            {
                return false;
            }

            var args = new[] { sourceProperty.GetValue(sourceObject) };
            if(_attribute.AdditionalArguments != null)
            {
                args = args.Concat(_attribute.AdditionalArguments).ToArray();
            }

            var newValue = methodAndConverter?.method.Invoke
            (
                methodAndConverter?.converter,
                args
                
            );

            targetProperty.SetValue(targetObject, newValue);
            return true;

        }

        protected override Type GetConverterType(Type targetType, Type sourceType)
        {
            var genericSimpleConverterType = (typeof(IConverter<,>)).MakeGenericType(sourceType, targetType);
            if (!genericSimpleConverterType.IsAssignableFrom(_attribute.ConverterType))
            {
                return null;
            }
            return _attribute.ConverterType;
        }

        protected override Type GetEnumerableConverterType(Type targetType, Type sourceType)
        {
            throw new NotImplementedException();
        }

        protected override string GetMethodName()
        {
            return _attribute.MethodName ?? "Convert"; 
        }
    }
}
