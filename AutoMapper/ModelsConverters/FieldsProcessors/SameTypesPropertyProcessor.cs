﻿using Automapper.Contracts;
using System.Reflection;


namespace Automapper.ModelsConverters.FieldsProcessors
{
    public class SameTypesPropertyProcessor : IPropertyProcessor
    {
        public bool ProcessProperty(PropertyInfo targetField, object sourceObject, object targetObject)
        {
            var sourceField = sourceObject.GetType().GetProperty(targetField.Name);
            if (sourceField == null)
            {
                return false;
            }

            if (!targetField.PropertyType.IsAssignableFrom(sourceField.PropertyType))
            {
                return false;
            }

            var value = sourceField.GetValue(sourceObject);
            targetField.SetValue(targetObject, value);
            return true;
        }
    }
}
