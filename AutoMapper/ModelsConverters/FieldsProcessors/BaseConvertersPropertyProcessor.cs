﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;


namespace Automapper.ModelsConverters.FieldsProcessors
{
    public abstract class BaseConvertersPropertyProcessor
    {
        protected abstract Type GetEnumerableConverterType(Type targetType, Type sourceType);
        protected abstract Type GetConverterType(Type targetType, Type sourceType);
        protected abstract string GetMethodName();


        private readonly IServiceProvider _serviceProvider;
        public BaseConvertersPropertyProcessor(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public (MethodInfo method, object converter)? GetConvertMethod(PropertyInfo targetProperty, PropertyInfo sourceProperty, object sourceObject, object targetObject)
        {
            if(targetProperty.Name == "Author")
            {

            }
            Type converterType;

            if (typeof(IEnumerable).IsAssignableFrom(targetProperty.PropertyType) && typeof(IEnumerable).IsAssignableFrom(sourceProperty.PropertyType))
            {
                var targetType = targetProperty.PropertyType.GetGenericArguments().First();
                var genericTargetIEnumerable = typeof(IEnumerable<>).MakeGenericType(targetType);
                if (targetProperty.PropertyType != genericTargetIEnumerable)
                {
                    throw new InvalidCastException("Target field should explicitly be declared as IEnumerable<>");
                }
                var sourceType = sourceProperty.PropertyType.GetGenericArguments().First();

                converterType = GetEnumerableConverterType(targetType, sourceType);
            }
            else
            {
                converterType = GetConverterType(targetProperty.PropertyType, sourceProperty.PropertyType);
            }

            if(converterType == null)
            {
                return null;
            }

            var converter = _serviceProvider.GetService(converterType);
            if(converter == null)
            {
                return null;
            }

            var methodName = GetMethodName();
            var convertMethod = converter.GetType().GetMethod(methodName);

            if(convertMethod == null)
            {
                throw new ArgumentException($"{converterType.ToString()} does not contain method {methodName}");
            }

            return (convertMethod, converter);
        }
    }
}
