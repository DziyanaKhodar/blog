﻿using Automapper.Contracts;
using Automapper.ModelsConverters.Attributes;
using System;
using System.Linq;
using System.Reflection;


namespace Automapper.ModelsConverters.FieldsProcessors
{
    public class NestedModelPropertyProcessor : BaseConvertersPropertyProcessor, IPropertyProcessor
    {
        public NestedModelPropertyProcessor(IServiceProvider serviceProvider): base(serviceProvider)
        {


        }

        public bool ProcessProperty(PropertyInfo targetProperty, object sourceObject, object targetObject)
        {
            var sourceProperty = sourceObject.GetType().GetProperty(targetProperty.Name);
            if (sourceProperty == null)
            {
                return false;
            }
            if (!targetProperty.GetCustomAttributes().Any(x => typeof(UseViewModelConverterAttribute).IsAssignableFrom(x.GetType())))
            {
                return false;
            }

            var methodAndConverter = GetConvertMethod(targetProperty, sourceProperty, sourceObject, targetObject);

            if(methodAndConverter == null)
            {
                throw new Exception("Required services for NestedModelPropertyProcessor are not registered.");
            }

            var newValue = methodAndConverter?.method.Invoke
            (
                methodAndConverter?.converter, 
                new[] { sourceProperty.GetValue(sourceObject) }
            );

            targetProperty.SetValue(targetObject, newValue);
            return true;

        }

        protected override Type GetConverterType(Type targetType, Type sourceType)
        {
            var converterType = typeof(IModelConverter<,>);
            return converterType.MakeGenericType(targetType, sourceType);
        }

        protected override Type GetEnumerableConverterType(Type targetType, Type sourceType)
        {
            var converterType = typeof(IModelEnumerableConverter<,>);
            return converterType.MakeGenericType(targetType, sourceType);
        }

        protected override string GetMethodName()
        {
            return "Convert";
        }
    }
}
