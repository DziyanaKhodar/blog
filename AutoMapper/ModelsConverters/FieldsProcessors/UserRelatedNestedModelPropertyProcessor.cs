﻿using Automapper.Contracts;
using Automapper.ModelsConverters.Attributes;
using System;
using System.Linq;
using System.Reflection;


namespace Automapper.ModelsConverters.FieldsProcessors
{
    public class UserRelatedNestedModelPropertyProcessor<TUser> : BaseConvertersPropertyProcessor, IUserRelatedPropertyProcessor<TUser>
    {

        public UserRelatedNestedModelPropertyProcessor(IServiceProvider serviceProvider): base(serviceProvider)
        {

        }

        public bool ProcessProperty(PropertyInfo targetField, object sourceObject, object targetObject, TUser user)
        {
            var sourceField = sourceObject.GetType().GetProperty(targetField.Name);

            if (sourceField == null)
            {
                return false;
            }
            if (!targetField.GetCustomAttributes().Any(x => typeof(UseViewModelConverterAttribute).IsAssignableFrom(x.GetType())))
            {
                return false;
            }

            var methodAndConverter = GetConvertMethod(targetField, sourceField, sourceObject, targetObject);

            if (methodAndConverter == null)
            {
                throw new Exception("Required services for UserRelatedNestedModelPropertyProcessor are not registered.");
            }

            var newValue = methodAndConverter?.method.Invoke
            (
                methodAndConverter?.converter,
                new[] { sourceField.GetValue(sourceObject), user }
            );

            targetField.SetValue(targetObject, newValue);
            return true;
        }

        protected override Type GetConverterType(Type targetType, Type sourceType)
        {
            var converterType = typeof(IUserRelatedModelConverter<,,>);
            return converterType.MakeGenericType(targetType, sourceType, typeof(TUser));
        }

        protected override Type GetEnumerableConverterType(Type targetType, Type sourceType)
        {
            var converterType = typeof(IUserRelatedModelEnumerableConverter<,,>);
            return converterType.MakeGenericType(targetType, sourceType, typeof(TUser));
        }

        protected override string GetMethodName()
        {
            return "ConvertUserRelated";
        }

    }
}
