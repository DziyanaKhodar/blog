﻿using Automapper.Contracts;
using System;
using System.Collections.Generic;
using System.Reflection;


namespace Automapper.ModelsConverters
{
    public class NameConventionsFieldsBinder : INameConventionsFieldsBinder
    {
        public object BindFields<T, U>(T source, U target, IEnumerable<IPropertyProcessor> propertyProcessors)
            where T : class
            where U : class
        {
            Action<PropertyInfo> action = field =>
            {
                foreach (var propertyProcessor in propertyProcessors)
                {
                    if (propertyProcessor.ProcessProperty(field, source, target))
                    {
                        break;
                    }
                }
            };

            return BindFields(source, target, action);
        }

        public object BindFields<T, U, TUser>(T source, U target, IEnumerable<IUserRelatedPropertyProcessor<TUser>> fieldProcessors, TUser user) where T:class where U:class
        {
            Action<PropertyInfo> action = field =>
            {
                foreach (var fieldProcessor in fieldProcessors)
                {
                    if (fieldProcessor.ProcessProperty(field, source, target, user))
                    {
                        break;
                    }
                }
            };

            return BindFields(source, target, action);
        }

        private object BindFields<T, U>(T source, U target, Action<PropertyInfo> action)
        {
            BindingFlags bf = BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly;
            var fields = typeof(U).GetProperties(bf);

            foreach (var field in fields)
            {
                action(field);
            }

            return target;
        }
    }
}
