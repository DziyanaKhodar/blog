﻿using Automapper.Contracts;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Automapper.ModelsConverters
{
    public class UserRelatedModelConverter<U, T, TUser> : ModelConverter<U,T>,  IUserRelatedModelUpdating<U, T, TUser> where U: class, new() where T: class
    {
        private readonly IEnumerable<IUserRelatedPropertyProcessor<TUser>> _userRelatedPropertyProcessors;

        public UserRelatedModelConverter
        (
            INameConventionsFieldsBinder binder,
            IEnumerable<IPropertyProcessor> propertyProcessors,
            IEnumerable<IUserRelatedPropertyProcessor<TUser>> userRelatedPropertyProcessors,
            IServiceProvider serviceProvider
        ) : base(binder, propertyProcessors, serviceProvider)
        {
            _userRelatedPropertyProcessors = userRelatedPropertyProcessors;
        }

        protected virtual void UpdateInternal(T entity, U initialTarget, TUser user)
        {     
            UpdateInternal(entity, initialTarget);
            _binder.BindFields(entity, initialTarget, _userRelatedPropertyProcessors, user);

        }

        protected new MethodInfo GetConverterMethod(object converter)
        {
            return converter.GetType().GetMethod("UpdateUserRelated", new[] { typeof(T), _baseClass, typeof(TUser) });
        }

        public virtual void UpdateUserRelated(T entity, U initialTarget, TUser user)
        {
            var baseClass = typeof(U).BaseType;

            if(baseClass == typeof(object))
            {
                UpdateInternal(entity, initialTarget, user);
                return;
            }

            var userRelatedConverterInterface = typeof(IUserRelatedModelUpdating<,,>);
            userRelatedConverterInterface = userRelatedConverterInterface.MakeGenericType(baseClass, typeof(T), typeof(TUser));

            var userRelatedService = _serviceProvider.GetService(userRelatedConverterInterface);
            var registeredUserRelatedConverterType = userRelatedService.GetType();

            var userRelatedConverterType = typeof(UserRelatedModelConverter<,,>)
                .MakeGenericType(baseClass, typeof(T), typeof(TUser));

            if (registeredUserRelatedConverterType != userRelatedConverterType)
            {
                var method = GetConverterMethod(userRelatedService);
                UpdateFromBaseClass(entity, initialTarget, method, userRelatedService, user);
            }
            else
            {
                var service = base.GetBaseClassConverter();
                var registeredConverterType = service.GetType();

                var converterType = typeof(ModelConverter<,>)
                    .MakeGenericType(baseClass, typeof(T));

                if (registeredConverterType != converterType)
                {
                    var method = base.GetConverterMethod(service);
                    UpdateFromBaseClass(entity, initialTarget, method, service);
                }
                else
                {
                    var method = GetConverterMethod(userRelatedService);
                    UpdateFromBaseClass(entity, initialTarget, method, userRelatedService, user);
                }
            }
            UpdateInternal(entity, initialTarget, user);
        }

        public virtual U ConvertUserRelated(T entity, TUser user)
        {
            var target = new U();
            UpdateUserRelated(entity, target, user);
            return target;
        }
    }
}
