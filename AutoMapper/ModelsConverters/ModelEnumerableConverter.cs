﻿using Automapper.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace Automapper.ModelsConverters
{
    public class ModelEnumerableConverter<T, U> : IModelEnumerableConverter<T, U> where T : class, new() where U : class
    {
        private readonly IModelUpdating<T, U> _viewModelsConverter;
        public ModelEnumerableConverter(IModelUpdating<T, U> viewModelsConverter)
        {
            _viewModelsConverter = viewModelsConverter;
        }
        public IEnumerable<T> Convert(IEnumerable<U> source)
        {
            return source.Select(x => _viewModelsConverter.Convert(x)).ToList();
        }
    }
}
