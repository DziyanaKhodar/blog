﻿using Automapper.Contracts;
using System;


namespace Automapper.ModelsConverters.FieldsConverters
{
    public class DateTimeFormatter : ISimpleDateTimeFormatter
    {
        public string Convert(DateTimeOffset? dateTime)
        {
            var converted = dateTime?.ToString("yyyy-MM-dd HH:mm:ss");
            if (converted == null)
                return string.Empty;
            return converted;
        }
    }
}
