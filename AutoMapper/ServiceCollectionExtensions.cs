﻿using Automapper.Contracts;
using Automapper.ModelsConverters;
using Automapper.ModelsConverters.FieldsConverters;
using Automapper.ModelsConverters.FieldsProcessors;
using Microsoft.Extensions.DependencyInjection;


namespace AutoMapper
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddAutoMapper<TUser, TPermissions>(this IServiceCollection services)
        {

            services.AddTransient<ISimpleDateTimeFormatter, DateTimeFormatter>();
            services.AddTransient<INameConventionsFieldsBinder, NameConventionsFieldsBinder>();

            services.AddTransient<IPropertyProcessor, SimpleConvertersPropertyProcessor>();
            services.AddTransient<IPropertyProcessor, SameTypesPropertyProcessor>();
            services.AddTransient<IUserRelatedPropertyProcessor<TUser>, UserRelatedNestedModelPropertyProcessor<TUser>>();
            services.AddTransient<IPropertyProcessor, NestedModelPropertyProcessor>();

            services.AddTransient<IUserRelatedPropertyProcessor<TUser>, PermissionsPropertyProcessor<TUser, TPermissions>>();

            services.AddTransient(typeof(IModelConverter<,>), typeof(ModelConverter<,>));
            services.AddTransient(typeof(IUserRelatedModelConverter<,,>), typeof(UserRelatedModelConverter<,,>));

            services.AddTransient(typeof(IUserRelatedModelEnumerableConverter<,,>), typeof(UserRelatedModelEnumerableConverter<,,>));
            services.AddTransient(typeof(IModelUpdating<,>), typeof(ModelConverter<,>));
            services.AddTransient(typeof(IUserRelatedModelUpdating<,,>), typeof(UserRelatedModelConverter<,,>));
            services.AddTransient(typeof(IModelEnumerableConverter<,>), typeof(ModelEnumerableConverter<,>));
            return services;
        }
    }
}
