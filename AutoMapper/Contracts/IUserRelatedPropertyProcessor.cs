﻿using System.Reflection;

namespace Automapper.Contracts
{
    public interface IUserRelatedPropertyProcessor<TUser>
    {
        bool ProcessProperty(PropertyInfo targetProperty, object sourceObject, object targetObject, TUser user);
    }
}
