﻿using System.Collections.Generic;


namespace Automapper.Contracts
{
    public interface IUserRelatedModelEnumerableConverter<out T, in U, TUser> where T: class,new() where U: class
    {
        IEnumerable<T> ConvertUserRelated(IEnumerable<U> source, TUser user);
    }
}
