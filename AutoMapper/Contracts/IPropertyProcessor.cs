﻿using System.Reflection;


namespace Automapper.Contracts
{
    public interface IPropertyProcessor
    {
        bool ProcessProperty(PropertyInfo targetProperty, object sourceObject, object targetObject);
    }
}
