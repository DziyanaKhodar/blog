﻿using System.Collections.Generic;


namespace Automapper.Contracts
{
    public interface INameConventionsFieldsBinder
    {
        object BindFields<T, U, TUser>(T source, U target, IEnumerable<IUserRelatedPropertyProcessor<TUser>> fieldConverters, TUser user) where T : class where U : class;
        object BindFields<T, U>(T source, U target, IEnumerable<IPropertyProcessor> propertyProcessors) where T : class where U : class;
    }
}
