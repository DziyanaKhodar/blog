﻿
namespace Automapper.Contracts
{
    public interface IModelConverter<U, in M> where U : class, new() where M : class
    {
        U Convert(M source);
        
    }
}
