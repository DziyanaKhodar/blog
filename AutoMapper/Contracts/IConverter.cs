﻿
namespace Automapper.Contracts
{
    public interface IConverter<in T, out U>
    {
        U Convert(T value);
    }
}
