﻿
namespace Automapper.Contracts
{
    public interface IUserRelatedModelUpdating<U, in M, TUser> : IUserRelatedModelConverter<U,M,TUser> where U : class, new() where M : class
    {
        void UpdateUserRelated(M entity, U initialTarget, TUser user);
    }
}
