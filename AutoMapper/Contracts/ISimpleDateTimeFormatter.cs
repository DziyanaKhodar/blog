﻿using System;

namespace Automapper.Contracts
{
    public interface ISimpleDateTimeFormatter : IConverter<DateTimeOffset?, string>
    {

    }
}