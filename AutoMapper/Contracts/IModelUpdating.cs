﻿

namespace Automapper.Contracts
{
    public interface IModelUpdating<U, in M>: IModelConverter<U,M> where U : class, new() where M : class
    {
        void Update(M entity, U initialTarget);
    }
}
