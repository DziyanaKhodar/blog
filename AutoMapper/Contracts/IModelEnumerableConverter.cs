﻿using System.Collections.Generic;


namespace Automapper.Contracts
{
    public interface IModelEnumerableConverter<out T, in U> where T : class, new() where U : class
    {
        IEnumerable<T> Convert(IEnumerable<U> source);
    }
}
