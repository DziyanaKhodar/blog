﻿
namespace Automapper.Contracts
{
    public interface IUserRelatedModelConverter<U, in M, TUser> where U : class, new() where M : class
    {
        U ConvertUserRelated(M source, TUser user);
        
    }
}
