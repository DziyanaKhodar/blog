﻿using Blog.Data.Models;
//using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Blog.Data.EntitiesConfiguration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace Blog.Data
{
    public class BlogDbContext : IdentityDbContext<User, Role, long, IdentityUserClaim<long>, UserRole, IdentityUserLogin<long>, IdentityRoleClaim<long>, IdentityUserToken<long>>
    {
        public BlogDbContext(DbContextOptions<BlogDbContext> options) : base(options)
        {

        }

        //public BlogDbContext()
        //{

        //}

        public virtual DbSet<Blogg> Blogs { get; set; }
        public virtual DbSet<Post> Posts { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<Tag> Tags { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer("Data Source=MY-PC\\SQLEXPRESS;Initial Catalog=TestBlog;Integrated Security=SSPI;");
            var loggerFactory = new LoggerFactory();
            loggerFactory.AddProvider(new DbFileLoggerProvider("logs"));
            optionsBuilder.UseLoggerFactory(loggerFactory);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);  
            builder.ApplyConfiguration(new UserEntityConfiguration());
            builder.ApplyConfiguration(new BlogEntityConfiguration());
            builder.ApplyConfiguration(new PostEntityConfiguration());
            builder.ApplyConfiguration(new CommentEntityConfiguration());
            builder.ApplyConfiguration(new TagEntityConfiguration());
            builder.ApplyConfiguration(new TagPostEntityConfiguration());
            builder.ApplyConfiguration(new UserRolesEntityConfiguration());
        }
    }
}
