﻿using System;
using System.IO;
using Microsoft.Extensions.Logging;

namespace Blog.Data
{
    internal class DbFileLogger : ILogger
    {
        private readonly string _categoryName;
        private readonly string _logFilePath;
        private static object _locker = new object();

        public DbFileLogger(string categoryName, string logFilePath)
        {
            _categoryName = categoryName;
            _logFilePath = logFilePath;
        }

        public IDisposable BeginScope<TState>(TState state) => null;

        public bool IsEnabled(LogLevel logLevel) => true;

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            if (string.IsNullOrEmpty(_logFilePath) == false)
            {
                lock (_locker)
                {
                    var mes = $"{ logLevel } { eventId.Id} { _categoryName}" + formatter(state, exception);
                    File.AppendAllText(_logFilePath, mes);
                }
            }
        }
    }

    internal class DbFileLoggerProvider : ILoggerProvider
    {
        private const string LogFileName = "DatabaseLogs.txt";
        private readonly string _logFilePath;
        private readonly ILogger _emergencyLog;

        public DbFileLoggerProvider(string logFilePath)
        {
            _logFilePath = GetLogfile(logFilePath);
        }

        private string GetLogfile(string logPath)
        {
            if (string.IsNullOrEmpty(logPath))
            {
                return string.Empty;
            }

            if (Directory.Exists(logPath))
                return Path.Combine(logPath, LogFileName);

            Directory.CreateDirectory(logPath);
            return Path.Combine(logPath, LogFileName);
        }

        public ILogger CreateLogger(string categoryName) => new DbFileLogger(categoryName, _logFilePath);

        public void Dispose()
        {
        }
    }
}
