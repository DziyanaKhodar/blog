﻿using Blog.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Blog.Data.EntitiesConfiguration
{
    public class PostEntityConfiguration : IEntityTypeConfiguration<Post>
    {
        public void Configure(EntityTypeBuilder<Post> builder)
        {
            builder.HasKey(p => p.Id).HasName("PostId");
            builder.HasMany(p => p.Comments)
                .WithOne(c => c.Post);
            builder.HasMany(t => t.TagPosts).WithOne(t => t.Post).HasForeignKey(t => t.PostId).IsRequired();
        }
    }
}
