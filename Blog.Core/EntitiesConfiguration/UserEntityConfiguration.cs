﻿using Blog.Data.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Blog.Data.EntitiesConfiguration
{
    public class UserEntityConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(u => u.Id).HasName("UserId");
            builder.HasIndex(u => u.UserName).IsUnique();
            builder.HasMany(x => x.Claims).WithOne().HasForeignKey(x => x.UserId);
        }
    }
}
