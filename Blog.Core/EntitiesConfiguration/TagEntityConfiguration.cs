﻿using Blog.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Blog.Data.EntitiesConfiguration
{
    public class TagEntityConfiguration : IEntityTypeConfiguration<Tag>
    {
        public void Configure(EntityTypeBuilder<Tag> builder)
        {
            builder.HasKey(t => t.Id).HasName("TagId");
            builder.HasIndex(t => t.TagText).IsUnique();
            builder.HasMany(t => t.TagPosts).WithOne(t => t.Tag).HasForeignKey(t => t.TagId).IsRequired();
        }
    }
}
