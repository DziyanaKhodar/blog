﻿using Blog.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Blog.Data.EntitiesConfiguration
{
    public class CommentEntityConfiguration : IEntityTypeConfiguration<Comment>
    {
        public void Configure(EntityTypeBuilder<Comment> builder)
        {
            builder.HasKey(c => c.Id).HasName("CommentId");
            builder.HasOne(c => c.Author)
                .WithMany(a => a.Comments).OnDelete(DeleteBehavior.SetNull);
        }
    }
}
