﻿using Blog.Data.Contracts;


namespace Blog.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork
        (
            BlogDbContext blogDbContext,
            IBlogRepository blogRepository,
            ICommentRepository commentRepository,
            IPostRepository postRepository,
            ITagRepository tagRepository,
            IUserRepository userRepository
        )
        {
            Context = blogDbContext;
            BlogRepository = blogRepository;
            CommentRepository = commentRepository;
            PostRepository = postRepository;
            TagRepository = tagRepository;
            UserRepository = userRepository;
        }

        public BlogDbContext Context { get; }

        public IBlogRepository BlogRepository { get; }

        public ICommentRepository CommentRepository { get; }

        public IPostRepository PostRepository { get; }

        public ITagRepository TagRepository { get; }

        public IUserRepository UserRepository { get; }

        public void Save()
        {
            Context.SaveChanges();
        }
    }
}
