﻿using Blog.Data.Contracts;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;


namespace Blog.Data.Models
{
    public class User : IdentityUser<long>, IEntity
    {
        public User()
        {    
            Blogs = new HashSet<Blogg>();
            Comments = new HashSet<Comment>();
        }
        public virtual ICollection<Blogg> Blogs { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public string AboutMyself { get; set; }
        public string PhotoPath { get; set; }
        public ICollection<UserRole> UserRoles { get; set; }
        public ICollection<IdentityUserClaim<long>> Claims { get; set; }
    }
}
