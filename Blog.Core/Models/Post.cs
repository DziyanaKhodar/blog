﻿using System;
using System.Collections.Generic;


namespace Blog.Data.Models
{
    public class Post : Entity
    {
        public Post()
        {
            TagPosts = new HashSet<TagPost>();
            Comments = new HashSet<Comment>();
        }
        public string PostTitle { get; set; }
        public string PostText { get; set; }
        public DateTimeOffset? DatePublished { get; set; }

        public long BlogId { get; set; }
        public Blogg Blog { get; set; }
        public ICollection<TagPost> TagPosts { get; set; }
        public ICollection<Comment> Comments { get; set; }
    }
}
