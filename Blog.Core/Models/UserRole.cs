﻿
using Microsoft.AspNetCore.Identity;

namespace Blog.Data.Models
{

    public class UserRole : IdentityUserRole<long>
    {
        public User User { get; set; }
        public Role Role { get; set; }
    }
}
