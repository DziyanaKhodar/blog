﻿using Blog.Data.Contracts;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blog.Data.Models
{
    public class Entity : IEntity
    {
        [Key]
        public long Id { get; set; }
    }
}
