﻿using System.Collections.Generic;


namespace Blog.Data.Models
{
    public class Tag : Entity
    {
        public Tag()
        {

            TagPosts = new HashSet<TagPost>();
        }

        public string TagText { get; set; }
        public ICollection<TagPost> TagPosts { get; set; }
    }

}
