﻿using System.ComponentModel.DataAnnotations.Schema;


namespace Blog.Data.Models
{
    public class TagPost
    {
        [Column("TagPost_TagId")]
        public long TagId { get; set; }
        [Column("TagPost_PostId")]
        public long PostId { get; set; }
        public Tag Tag { get; set; }
        public Post Post { get; set; }
    }
}
