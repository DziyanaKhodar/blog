﻿using System;


namespace Blog.Data.Models
{
    public class Comment : Entity
    {
        public string CommentText { get; set; }
        public DateTimeOffset? DatePublished { get; set; }

        public long? AuthorId { get; set; }
        public long PostId { get; set; }
        public User Author { get; set; }
        public Post Post { get; set; }
    }
}
