﻿using System.Collections.Generic;


namespace Blog.Data.Models
{
    public class Blogg : Entity
    {
        public Blogg()
        {
            Posts = new HashSet<Post>();
        }

        public string BlogTitle { get; set; }

        public long OwnerId { get; set; }
        public User Owner { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
        public string PhotoPath { get; set; }
    }
    
}
