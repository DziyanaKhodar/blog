﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Blog.Migrations
{
    public partial class UniqueConstraintOnTagText : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TagId",
                table: "Tags");

            migrationBuilder.DropColumn(
                name: "PostId",
                table: "Posts");

            migrationBuilder.AlterColumn<string>(
                name: "TagText",
                table: "Tags",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Tags_TagText",
                table: "Tags",
                column: "TagText",
                unique: true,
                filter: "[TagText] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Tags_TagText",
                table: "Tags");

            migrationBuilder.AlterColumn<string>(
                name: "TagText",
                table: "Tags",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "TagId",
                table: "Tags",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "PostId",
                table: "Posts",
                nullable: false,
                defaultValue: 0L);
        }
    }
}
