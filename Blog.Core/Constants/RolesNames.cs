﻿

namespace Blog.Data.Constants
{
    public static class RolesNames
    {
        public const string Admin = "ADMIN";
        public const string Moderator = "MODERATOR";
    }
}
