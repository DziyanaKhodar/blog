﻿using System.Collections.Generic;
using System.Linq;
using Blog.Data.Models;


namespace Blog.Data.Contracts
{
    public interface IPostRepository : IRepository<Post>
    {
        IEnumerable<Post> GetPostsByAuthor(string userName, int? page = null);
        IEnumerable<Post> GetPostsByAuthor(long userId, int? page = null);
        IEnumerable<Post> GetPostsByTagsOrWords(string[] tags, string[] words, int? page = null);
        IEnumerable<Post> GetUserPostsByTagsOrWords(string userName, string[] tags, string[] words, int? page = null);
        IEnumerable<Post> GetBlogPostsByTagsOrWords(long blogId, string[] tags, string[] words, int? page = null);
        IEnumerable<Post> GetPostsByTagsOrWordsUserId(long userId, string[] tags, string[] words, int? page = null);
        IEnumerable<Post> GetPostsByTag(long tagId, int? page = null);
        IEnumerable<Post> All(int? page = null);
        IEnumerable<Post> GetPostsByBlogId(long blogId, int? page = null);

        IQueryable<Post> OrderByDatePublished(IQueryable<Post> posts);
        IQueryable<Post> GetPostsByBlogId(long blogId);
        IQueryable<Post> GetUserPostsByTagsOrWords(string userName, string[] tags, string[] words);
        IQueryable<Post> GetBlogPostsByTagsOrWords(long blogId, string[] tags, string[] words);
        IQueryable<Post> GetPostsByTagsOrWordsUserId(long userId, string[] tags, string[] words);
        IQueryable<Post> GetPostsByTagsOrWords(string[] tags, string[] words);
        IQueryable<Post> GetPostsByTagsIds(long[] ids);

        int Amount(string methodName, params object[] parameters);
        Post GetByIdNoIncludes(long id);
        Post GetLatestBlogPost(long id);
    }
}
