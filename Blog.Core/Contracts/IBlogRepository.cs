﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Blog.Data.Models;


namespace Blog.Data.Contracts
{
    public interface IBlogRepository : IRepository<Blogg>
    {
        IEnumerable<Blogg> GetBlogsByTitle(string title, int? page = null);
        Task<IEnumerable<Blogg>> GetBlogsByOwner(long userId);
        IEnumerable<Blogg> GetBlogByOwnerName(string userName, int? page = null);
        IEnumerable<Blogg> All(int? page = null);
        int GetPostsAmount(long blogId);
    }
}
