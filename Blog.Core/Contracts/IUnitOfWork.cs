﻿using System;


namespace Blog.Data.Contracts
{
    public interface IUnitOfWork
    {
        void Save();
        IBlogRepository BlogRepository { get; }
        ICommentRepository CommentRepository { get; }
        IPostRepository PostRepository { get; }
        ITagRepository TagRepository { get; }
        IUserRepository UserRepository { get; }
        BlogDbContext Context { get; }
    }
}
