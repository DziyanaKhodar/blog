﻿using Blog.Data.Models;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace Blog.Data.Contracts
{
    public interface IUserRepository : IRepository<User> 
    {
        User GetByUserName(string username);
        IEnumerable<string> GetUserRoles(long id);
        bool IsBanned(long id);
        IEnumerable<IdentityUserClaim<long>> GetUserClaims(string username);
    }
}
