﻿using System.Collections.Generic;
using Blog.Data.Models;


namespace Blog.Data.Contracts
{
    public interface ICommentRepository : IRepository<Comment>
    {
        IEnumerable<Comment> GetUserComments(long userId, int? page = null);
        IEnumerable<Comment> GetPostComments(long postId);

    }
}
