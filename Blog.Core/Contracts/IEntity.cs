﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blog.Data.Contracts
{
    public interface IEntity
    {
        long Id { get; set; }
    }
}
