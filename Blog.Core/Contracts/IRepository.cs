﻿using Blog.Data.Models;


namespace Blog.Data.Contracts
{
    public interface IRepository<T> where T:IEntity
    {
        T GetById(long id);
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        void CreateOrUpdate(T entity);
    }
}
