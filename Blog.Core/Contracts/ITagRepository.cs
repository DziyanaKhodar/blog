﻿using System.Collections.Generic;
using Blog.Data.Models;


namespace Blog.Data.Contracts
{
    public interface ITagRepository : IRepository<Tag>
    {
        IEnumerable<Tag> GetPostTags(long postId);
        Tag GetTagByText(string text);
        IEnumerable<Tag> GetTagsBatch(long[] ids);
        IEnumerable<Tag> GetTagsByFirstLetters(string firstLetters);
    }
}
