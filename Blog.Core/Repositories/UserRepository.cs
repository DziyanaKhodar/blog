﻿using System.Linq;
using Blog.Data.Models;
using Blog.Data.Contracts;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Blog.Data.Constants;
using System;
using Microsoft.AspNetCore.Identity;

namespace Blog.Data.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(BlogDbContext blogDbContext) : base(blogDbContext)
        {

        }

        private IQueryable<User> IncludeNavigationFields(IQueryable<User> users)
        {
            return users
                .Include(y => y.Blogs)
                .Include(y => y.Claims)
                .Include(y => y.UserRoles)
                .ThenInclude(x => x.Role);
        }

        public override User GetById(long id)
        {
            return IncludeNavigationFields(_context.Users.Where(x => x.Id == id))
                .FirstOrDefault();
        }

        public User GetByUserName(string username)
        {
            return IncludeNavigationFields(_context.Users.Where(u => u.UserName == username)).FirstOrDefault();
        }

        public IEnumerable<string> GetUserRoles(long id)
        {
            return _context.Users.Where(u => u.Id == id).SelectMany(u => u.UserRoles).Select(x => x.Role).Select(x => x.NormalizedName);
        }

        public bool IsBanned(long id)
        {
            var claims = _context.Users.Where(u => u.Id == id).SelectMany(u => u.Claims).Select(x => x.ClaimType);
            return claims.Any(x => string.Equals(x, ClaimsTypes.Banned, StringComparison.OrdinalIgnoreCase));
        }

        public IEnumerable<IdentityUserClaim<long>> GetUserClaims(string username)
        {
            return _context.Users.Where(u => u.UserName == username).SelectMany(x => x.Claims);
        }
    }
}
