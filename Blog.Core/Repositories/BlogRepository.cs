﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blog.Data.Models;
using Blog.Data.Contracts;
using Microsoft.EntityFrameworkCore;


namespace Blog.Data.Repositories
{
    public class BlogRepository : Repository<Blogg>, IBlogRepository
    {
        public BlogRepository(BlogDbContext dbContext):base(dbContext)
        {

        }

        private IQueryable<Blogg> IncludeOwner(IQueryable<Blogg> blogs)
        {
            return blogs.Include(x => x.Owner);
        }

        public IEnumerable<Blogg> All(int? page = null)
        {
            return GetPage(IncludeOwner(_context.Blogs), page);
        }

        public override Blogg GetById(long id)
        {
            return IncludeOwner(_context.Blogs.Where(x => x.Id == id)).FirstOrDefault();
        }

        public IEnumerable<Blogg> GetBlogByOwnerName(string userName, int? page = null)
        {
            var result = _context.Blogs.Where(blog => blog.Owner.UserName.Contains(userName));
            return GetPage(result, page);
        }

        public async Task<IEnumerable<Blogg>> GetBlogsByOwner(long userId)
        {
            var allBlogs =_context.Blogs.Where(b => b.OwnerId == userId);
            return await allBlogs.ToArrayAsync();
        }

        public IEnumerable<Blogg> GetBlogsByTitle(string titlePart, int? page = null)
        {
            var allBlogs = _context.Blogs.Where(b => b.BlogTitle.Contains(titlePart));
            return GetPage(allBlogs);
        }

        public int GetPostsAmount(long blogId)
        {
            return _context.Blogs.Where(b => b.Id == blogId).SelectMany(b => b.Posts).Count();
        }
    }
}
