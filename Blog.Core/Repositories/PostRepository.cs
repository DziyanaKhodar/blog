﻿using Blog.Data.Contracts;
using Blog.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;


namespace Blog.Data.Repositories
{
    public class PostRepository : Repository<Post>, IPostRepository
    {
        public PostRepository(BlogDbContext blogDbContext) : base(blogDbContext)
        {

        }

        public int Amount(string methodName, params object[] parameters)
        {
            var filterMethodName = Regex.Replace(methodName, "^Get", "Filter");
            var methodInfo = this.GetType().GetMethod(filterMethodName, BindingFlags.NonPublic | BindingFlags.Instance);
            if (methodInfo == null)
            {
                throw new ArgumentException($"Amount method not supported for {methodName}");
            }
            var objects = methodInfo.Invoke(this, parameters.ToList().Prepend(_context.Posts).ToArray());
            return ((IQueryable<Post>)objects).Count();
        }

        public IQueryable<Post> GetPostsByTagsIds(long[] ids)
        {
            return OrderByDatePublished(_context.Tags
                .Where(t => ids.Contains(t.Id))
                .SelectMany(tp => tp.TagPosts)
                .Select(tp => tp.Post)
                .Distinct())
                .Include(x =>x.Blog)
                .ThenInclude(y => y.Owner)
                .Include(x => x.TagPosts)
                .ThenInclude(y => y.Tag);
        }

        public IQueryable<Post> OrderByDatePublished(IQueryable<Post> posts)
        {
            return posts.OrderByDescending(x => x.DatePublished).ThenBy(x => x.Id);
        }

        public Post GetLatestBlogPost(long id)
        {
            return IncludeAllFields(_context.Posts
                .Where(x => x.BlogId == id))
                .OrderByDescending(x => x.DatePublished)
                .FirstOrDefault();
        }

        public Post GetByIdNoIncludes(long id)
        {
            return base.GetById(id);
        }

        public override Post GetById(long id)
        {
            return IncludeAllFields(_context.Posts
                .Where(x => x.Id == id))
                
                .FirstOrDefault();
        }

        private IQueryable<Post> IncludeAllFields(IQueryable<Post> posts)
        {
            return posts
                .Include(x => x.Comments).ThenInclude(c => c.Author)
                .Include(x => x.Blog).ThenInclude(b => b.Owner)
                .Include(x => x.TagPosts).ThenInclude(tp => tp.Tag);
        }

        public IEnumerable<Post> All(int? page = null)
        {
            return GetPage(IncludeAllFields(_context.Posts), page);
        }

        public IEnumerable<Post> GetPostsByTag(long tagId, int? page = null)
        {
            var result = FilterPostsByTag(tagId);
            return GetPage(IncludeAllFields(result), page);
        }

        public IEnumerable<Post> GetPostsByAuthor(long userId, int? page = null)
        {
            var result = FilterPostsByAuthor(_context.Posts, userId).Include("Blog.Owner");
            return GetPage(IncludeAllFields(result), page);
        }

        public IEnumerable<Post> GetPostsByTagsOrWordsUserId(long userId, string[] tags, string[] words, int? page = null)
        {     
            return GetPage(GetPostsByTagsOrWordsUserId(userId, tags, words), page);
        }

        public IEnumerable<Post> GetPostsByBlogId(long blogId, int? page = null)
        {
            return GetPage(GetPostsByBlogId(blogId), page);
        }

        public IEnumerable<Post> GetBlogPostsByTagsOrWords(long blogId, string[] tags, string[] words, int? page = null)
        {      
            return GetPage(GetBlogPostsByTagsOrWords(blogId, tags, words), page);
        }

        public IEnumerable<Post> GetUserPostsByTagsOrWords(string userName, string[] tags, string[] words, int? page = null)
        {
            return GetPage(GetUserPostsByTagsOrWords(userName, tags, words), page);
        }

        public IEnumerable<Post> GetPostsByAuthor(string userName, int? page = null)
        {
            var result = FilterPostsByAuthor(_context.Posts, userName);
            return GetPage(IncludeAllFields(result), page);
        }

        public IEnumerable<Post> GetPostsByTagsOrWords(string[] tags, string[] words, int? page)
        {   
            var res = GetPage(GetPostsByTagsOrWords(tags,words), page);
            return res;
        }

        public IQueryable<Post> GetPostsByBlogId(long blogId)
        {
            return IncludeAllFields(OrderByDatePublished(FilterPostsByBlogId(_context.Posts, blogId)));
        }

        public IQueryable<Post> GetUserPostsByTagsOrWords(string userName, string[] tags, string[] words)
        {
            var result = FilterUserPostsByTagsOrWords(_context.Posts, userName, tags, words);
            return IncludeAllFields(OrderByDatePublished(result));
        }

        public IQueryable<Post> GetBlogPostsByTagsOrWords(long blogId, string[] tags, string[] words)
        {
            var result = FilterBlogPostsByTagsOrWords(_context.Posts, blogId, tags, words);
            return IncludeAllFields(OrderByDatePublished(result));
        }

        public IQueryable<Post> GetPostsByTagsOrWordsUserId(long userId, string[] tags, string[] words)
        {
            var result = FilterPostsByTagsOrWordsUserId(_context.Posts, userId, tags, words);
            return IncludeAllFields(OrderByDatePublished(result));
        }

        public IQueryable<Post> GetPostsByTagsOrWords(string[] tags, string[] words)
        {
            var result = FilterPostsByTagsOrWords(_context.Posts, tags, words);
            return IncludeAllFields(OrderByDatePublished(result));
        }

        private IQueryable<Post> FilterUserPostsByTagsOrWords(IQueryable<Post> posts,string userName, string[] tags, string[] words)
        {
            var result = FilterPostsByTagsOrWords(posts, tags, words);
            var authorPosts = FilterPostsByAuthor(result, userName);
            return authorPosts;
        }

        private IQueryable<Post> FilterBlogPostsByTagsOrWords(IQueryable<Post> posts, long blogId, string[] tags, string[] words)
        {
            var result = FilterPostsByTagsOrWords(posts, tags, words);
            var blogPosts = FilterPostsByBlogId(result, blogId);
            return blogPosts;       
        }

        private IQueryable<Post> FilterPostsByTagsOrWordsUserId(IQueryable<Post> posts, long userId, string[] tags, string[] words)
        {
            var authorPosts = FilterPostsByAuthor(posts, userId);
            var result = FilterPostsByTagsOrWords(authorPosts, tags, words);
            return result;
        }
        
        private IQueryable<Post> FilterPostsByTag(long tagId)
        {
            var result =_context.Posts.Where(p => p.TagPosts.Any(tp => tp.TagId == tagId));
            return result;
        }

        private IQueryable<Post> FilterPostsByBlogId(IQueryable<Post> posts, long blogId)
        {
            var result = posts.Where(p => p.BlogId == blogId);
            return result;
        }

        private IQueryable<Post> FilterPostsByAuthor(IQueryable<Post> posts, long userId)
        {
            var result = posts.Where(p => p.Blog.OwnerId == userId);
            return result;
        }
    
        private IQueryable<Post> FilterPostsByAuthor(IQueryable<Post> posts, string userName)
        {
            return posts.Where(p => p.Blog.Owner.UserName.Contains(userName));
        }

        private IQueryable<Post> FilterPostsByTagsOrWords(IQueryable<Post> posts, string[] tags, string[] words)
        {
            if(!tags.Any() && !words.Any())
            {
                return posts;
            }
            var query = GetTagsAndWordsSearchQuery(words, tags);
            var result = posts.FromSql(query);
            return result;
        }

        private string GetTextSearchQuery(string likeQuery, string[] words, int searchFieldsAmount)
        {
            if(words.All(x => string.IsNullOrWhiteSpace(x)))
            {
                return string.Empty;
            }
            words = words.Select(w => "'%" + w.Trim() + "%'").Distinct(StringComparer.Ordinal).ToArray();
            var likeQueries = words.Select(w => string.Format(likeQuery, Enumerable.Repeat(w,searchFieldsAmount).ToArray())).ToArray();

            return string.Join(" OR ", likeQueries);
        }

        private string ConcatTextSearchQueries(string baseQuery, string delimeter, params string[] likeQueries)
        {
            var notEmptyLikeQueries = likeQueries.Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();
            if(!notEmptyLikeQueries.Any())
            {
                return string.Concat(baseQuery, "1=1");
            }

            return string.Concat(baseQuery, string.Join(delimeter, notEmptyLikeQueries));
        }

        private string GetTagsAndWordsSearchQuery(string[] words, string[] tags)
        {
            var basequery = @"SELECT DISTINCT [dbo].[Posts].*
                    FROM [dbo].[Posts]
                    LEFT JOIN [dbo].[TagPost] ON [dbo].[TagPost].TagPost_PostId = [dbo].[Posts].Id
                    LEFT JOIN [dbo].[Tags] ON [dbo].[TagPost].TagPost_TagId = [dbo].[Tags].Id
                    JOIN [dbo].[Blogs] ON [dbo].[Posts].BlogId = [dbo].[Blogs].Id
                    WHERE ";
            var wordslikeQuery = "PostText LIKE {0} OR PostTitle LIKE {1} OR BlogTitle LIKE {2}";
            var wordsSearchQuery = GetTextSearchQuery(wordslikeQuery, words, 3);
            var tagLikeQuery = "TagText LIKE {0}";
            var tagsSearchQuery = GetTextSearchQuery(tagLikeQuery, tags, 1);

            return ConcatTextSearchQueries(basequery, " OR ", wordsSearchQuery, tagsSearchQuery);
        }
    }
}
