﻿using System.Collections.Generic;
using System.Linq;
using Blog.Data.Contracts;
using Blog.Data.Models;

namespace Blog.Data.Repositories
{
    public abstract class Repository<T> : IRepository<T> where T : class, IEntity
    {
        protected int PageSize = 10;
        protected BlogDbContext _context;

        public Repository(BlogDbContext context)
        {
            _context = context;
        }

        public virtual T GetById(long id)
        {
            return _context.Find<T>(id);
        }

        public void Add(T entity)
        {
            _context.Add(entity);
        }

        public void Update(T entity)
        {
            _context.Update(entity);
        }

        public void CreateOrUpdate(T entity)
        {
            if(entity.Id < 0)
            {
                Add(entity);
            }
            else
            {
                Update(entity);
            }
        }

        public void Delete(long id)
        {

        }

        public void Delete(T entity)
        {
            _context.Remove(entity);
        }

        protected IEnumerable<T> GetPage(IQueryable<T> all, int? page = null)
        {
            if (page == null || page <= 0)
                return all.ToList();
            return all
                .OrderBy(x => x.Id)
                .Skip(((int)page - 1) * PageSize)
                .Take((int)page * PageSize).ToList();
        }
    }
}
