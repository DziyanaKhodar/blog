﻿using System.Collections.Generic;
using System.Linq;
using Blog.Data.Models;
using Blog.Data.Contracts;
using Microsoft.EntityFrameworkCore;


namespace Blog.Data.Repositories
{
    public class CommentRepository : Repository<Comment>, ICommentRepository
    {
        public CommentRepository(BlogDbContext blogDbContext) : base(blogDbContext)
        {

        }

        public override Comment GetById(long id)
        {
            return _context.Comments.Where(x => x.Id == id).Include(x => x.Author)
                .Include(x => x.Post)
                .ThenInclude(x => x.Blog)
                .ThenInclude(x => x.Owner)
                .FirstOrDefault();
        }

        public IEnumerable<Comment> GetPostComments(long postId)
        {
            return _context.Comments.Where(x => x.PostId == postId).Include(x => x.Author).ToArray();
        }

        public IEnumerable<Comment> GetUserComments(long userId, int? page = null)
        {
            var res = _context.Comments.Where(x => x.AuthorId == userId);
            return GetPage(res, page);
        }
    }
}
