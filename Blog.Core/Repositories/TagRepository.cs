﻿using System;
using System.Collections.Generic;
using System.Linq;
using Blog.Data.Models;
using Blog.Data.Contracts;


namespace Blog.Data.Repositories
{
    public class TagRepository : Repository<Tag>, ITagRepository
    {
        public TagRepository(BlogDbContext blogDbContext):base(blogDbContext)
        {

        }

        //public IEnumerable<Post> GetTagPosts(long tagId, int page? = null)
        //{
        //    var result = GetById(tagId);
        //    if (result == null)
        //        return Enumerable.Empty<Post>();
        //    return GetPag
        //}

        public IEnumerable<Tag> GetTagsByFirstLetters(string firstLetters)
        {
            return _context.Tags.Where(t => t.TagText.StartsWith(firstLetters)).ToArray();
        }

        public IEnumerable<Tag> GetTagsBatch(long[] ids)
        {
            return _context.Tags.Where(t => ids.Contains(t.Id)).ToArray();
        }

        public IEnumerable<Tag> GetPostTags(long postId)
        {
            return _context.Tags.Where(t => t.TagPosts.Any(tp => tp.PostId == postId));
        }

        public Tag GetTagByText(string text)
        {
            return _context.Tags.Where(t => t.TagText == text).FirstOrDefault();
        }
    }
}
