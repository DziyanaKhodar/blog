﻿using Blog.Lib.Contracts;
using Blog.Data.Models;
using Blog.Data.Contracts;
using System.Collections.Generic;
using System.Linq;
using Blog.Data.Constants;
using System;


namespace Blog.Lib
{
    public class Permissions : IPermissions
    {
        private readonly IUnitOfWork _unitOfWork;
        public Permissions(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        //public IEnumerable<UserRoleActions> GetPossibleUserRoleActions(long userid)
        //{
        //    var actions = new List<UserRoleActions>();
        //    var user = _unitOfWork.UserRepository.GetById(userid);
        //    if (user == null)
        //    {
        //        return actions;
        //    }

        //    var userRoles = GetUserRoles(user);

        //    if (IsAdmin(userRoles))
        //    {
        //        return actions;
        //    }

        //    if (IsModerator(userRoles))
        //    {
        //        actions.AddRange(new[] { UserRoleActions.MakeAdmin, UserRoleActions.RemoveModeratorPriveleges });
        //        return actions;
        //    }

        //    actions.AddRange(new[] { UserRoleActions.MakeAdmin, UserRoleActions.MakeModerator });
        //    return actions;
        //}

        public bool CanAddComment(Post post, User requester)
        {
            return !IsBanned(requester);
        }

        public bool CanDeleteComment(Comment comment, User requester)
        {
            if(IsBanned(requester))
            {
                return false;
            }

            return (comment.AuthorId == requester.Id || CanEditPost(comment.Post, requester));
        }

        public bool CanEditPost(Post post, User requester)
        {
            return CanEditBlog(post.Blog, requester) || IsModerator(GetUserRoles(requester));
        }

        public bool CanEditUser(User user, User requester)
        {
            return user.Id == requester.Id;
        }

        public bool CanEditBlog(Blogg blog, User requester)
        {
            if (IsBanned(requester))
            {
                return false;
            }

            return blog.OwnerId == requester.Id;
        }

        public bool CanBanUser(long userid, long requesterid)
        {
            if (IsBanned(requesterid))
            {
                return false;
            }
            var userRoles = _unitOfWork.UserRepository.GetUserRoles(requesterid);
            var bannedUserRoles = _unitOfWork.UserRepository.GetUserRoles(userid);
            return CanBanInternal(bannedUserRoles, userRoles);
        }

        public bool CanBan(User user, User requester)
        {
            if (IsBanned(user))
            {
                return false;
            }
            var userRoles = GetUserRoles(requester);
            var bannedUserRoles = GetUserRoles(user);
            return CanBanInternal(bannedUserRoles, userRoles);
                
        }

        public bool CanRemoveUserFromBan(long userid, long requesterid)
        {
            if (!IsBanned(userid))
            {
                return false;
            }
            var userRoles = _unitOfWork.UserRepository.GetUserRoles(requesterid);
            return IsBanned(userid) && (IsModerator(userRoles) || IsAdmin(userRoles));
        }

        public bool CanRemoveFromBan(User user, User requester)
        {
            if (!IsBanned(user))
            {
                return false;
            }
            var userRoles = GetUserRoles(requester);
            return IsBanned(user) && ( IsModerator(userRoles) || IsAdmin(userRoles) );
        }

        public bool CanChangeUserRole(long userid, long requesterid)
        {
            var userRoles = _unitOfWork.UserRepository.GetUserRoles(userid);
            var requesterRoles = _unitOfWork.UserRepository.GetUserRoles(requesterid);
            return CanChangeRoleInternal(userRoles, requesterRoles, userid, requesterid);
        }

        public bool CanChangeRole(User user, User requester)
        {
            var requesterRoles = GetUserRoles(requester);
            var userRoles = GetUserRoles(user);
            return CanChangeRoleInternal(userRoles, requesterRoles, user.Id, requester.Id);
        }

        private bool CanChangeRoleInternal(IEnumerable<string> userRoles, IEnumerable<string> requesterRoles, long userid, long requesterid)
        {
            return IsAdmin(requesterRoles) && (!IsAdmin(userRoles) || userid == requesterid);
        }

        private bool CanBanInternal(IEnumerable<string> bannedUserRoles, IEnumerable<string> userRoles)
        {
            if (IsAdmin(bannedUserRoles))
            {
                return false;
            }
            return IsAdmin(userRoles) || IsModerator(userRoles) && !IsModerator(bannedUserRoles);
        }
        
        private bool RolesContain(string role, IEnumerable<string> roles)
        {
            return roles.Any(x => x.Equals(role, StringComparison.OrdinalIgnoreCase));
        }


        private IEnumerable<string> GetUserRoles(User user)
        {
            return user.UserRoles.Select(x => x.Role).Select(x => x.Name);
        }

        private bool IsBanned(long userid)
        {
            return _unitOfWork.UserRepository.IsBanned(userid);
        }

        private bool IsBanned(User user)
        {
            var claims = user.Claims.Select(x => x.ClaimType);
            return claims.Any(x => string.Equals(x, ClaimsTypes.Banned, StringComparison.OrdinalIgnoreCase));
        }

        private bool IsModerator(IEnumerable<string> roles)
        {
            return RolesContain(RolesNames.Moderator, roles);
        }

        private bool IsAdmin(IEnumerable<string> roles)
        {
            return RolesContain(RolesNames.Admin, roles);
        }


    }
}
