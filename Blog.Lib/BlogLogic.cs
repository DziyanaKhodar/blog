﻿using System;
using System.Collections.Generic;
using System.Linq;
using Blog.Data.Contracts;
using Blog.Data.Models;
using Blog.Lib.Contracts;
using Microsoft.EntityFrameworkCore;

namespace Blog.Lib
{
    public class BlogLogic : IBlogLogic
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPermissions _permissions;

        public BlogLogic(IUnitOfWork unitOfWork, IPermissions permissions)
        {
            _unitOfWork = unitOfWork;
            _permissions = permissions;
        }

        public bool IsExisting(long id)
        {
            return id > 0;
        }

        public bool IsExisting(IEntity entity)
        {
            return IsExisting(entity.Id);
        }

        private User GetUser(long id)
        {
            return _unitOfWork.UserRepository.GetById(id);
        }

        public bool UpdateUser(User user, long requester)
        {
            if (!_permissions.CanEditUser(user, GetUser(requester)))
            {
                return false;
            }
            _unitOfWork.UserRepository.Update(user);
            _unitOfWork.Save();
            return true;
        }

        public bool AddOrUpdateBlog(Blogg blog, long user)
        {
            if(!IsExisting(blog))
            {
                blog.OwnerId = user;
            }
            else if (!_permissions.CanEditBlog(blog, GetUser(user)))
            {
                return false;
            }
            _unitOfWork.BlogRepository.CreateOrUpdate(blog);
            _unitOfWork.Save();
            return true;
        }

        public bool DeleteBlog(Blogg blog, long user)
        {
            if (_permissions.CanEditBlog(blog, GetUser(user)))
            {
                _unitOfWork.BlogRepository.Delete(blog);
                _unitOfWork.Save();
                return true;
            }
            return false;
        }

        public bool AddPost(Post post, long user)
        {
            var blog = _unitOfWork.BlogRepository.GetById(post.BlogId);
            if (blog == null || !_permissions.CanEditBlog(blog, GetUser(user)))
            {
                return false;
            }
            _unitOfWork.PostRepository.Add(post);
            _unitOfWork.Save();
            return true;
        }

        public bool UpdatePost(Post post, long user)
        {
            if (!_permissions.CanEditPost(post, GetUser(user)))
            {
                return false;
            }
            _unitOfWork.PostRepository.Update(post);
            _unitOfWork.Save();
            return true;
        }

        public bool DeletePost(Post post, long user)
        {
            if (!_permissions.CanEditPost(post, GetUser(user)))
            {
                return false;
            }
            _unitOfWork.PostRepository.Delete(post);
            _unitOfWork.Save();
            return true;
        }

        public bool DeleteComment(Comment comment, long user)
        {
            if (!_permissions.CanDeleteComment(comment, GetUser(user)))
            {
                return false;
            }
            _unitOfWork.CommentRepository.Delete(comment);
            _unitOfWork.Save();
            return true;
        }

        public IQueryable<Post> SearchPostsByUserTagsOrWords(string userName, string[] tags, string[] words, long? blogId, long? userId)
        {
            IQueryable<Post> posts;
            if(blogId != null && blogId > 0)
            {
                posts = _unitOfWork.PostRepository
                    .GetBlogPostsByTagsOrWords(
                        (long)blogId,
                        tags,
                        words
                    );
            }
            else if(userId != null && userId > 0)
            {
                posts = _unitOfWork.PostRepository
                    .GetPostsByTagsOrWordsUserId(
                        (long)userId,
                        tags,
                        words
                    );
            }
            else if (string.IsNullOrWhiteSpace(userName))
            {
                posts = _unitOfWork.PostRepository
                    .GetPostsByTagsOrWords(
                        tags,
                        words
                    );
            }
            else
            {
                posts = _unitOfWork.PostRepository
                    .GetUserPostsByTagsOrWords(
                        userName.Trim(),
                        tags,
                        words
                    );

            }

            return posts;
        }

        public void PreservePostComments(Post post, IEnumerable<long> commentsIds)
        {
            if (post == null || commentsIds == null)
                return;

            post.Comments = post.Comments.Where(x => commentsIds.Contains(x.Id)).ToList();
        }

        public void AddTagsToPost(Post post, IEnumerable<(string text, long id)> tags)
        {
            if (post == null || tags == null)
                return;

            var distinctTags = tags.Distinct();
            var tagsToRemove = post.TagPosts
                .Where(x => !distinctTags.Select(y => y.text)
                .Contains(x.Tag.TagText)).ToArray();
            foreach (var tagPost in tagsToRemove)
            {
                post.TagPosts.Remove(tagPost);
            }

            var tagsToAdd = distinctTags.Where(x => !post.TagPosts.Select(y => y.Tag.TagText).Contains(x.text));

            foreach (var tag in tagsToAdd)
            {

                Tag tagToAdd = new Tag
                {
                    TagText = tag.text,
                };

                if (!IsExisting(tag.id))
                {
                    var existingTag = _unitOfWork.TagRepository.GetTagByText(tag.text);
                    if (existingTag != null)
                    {
                        tagToAdd = existingTag;
                    }
                }
                else
                {
                    tagToAdd.Id = tag.id;
                    _unitOfWork.Context.Entry(tagToAdd).State = EntityState.Unchanged;
                }

                post.TagPosts.Add(
                    new TagPost
                    {
                        Post = post,
                        Tag = tagToAdd
                    });
            }

        }

    }
}
