﻿using Blog.Data.Models;

namespace Blog.Lib.Contracts
{
    public interface IPermissions
    {
        bool CanDeleteComment(Comment comment, User requester);
        bool CanEditPost(Post post, User requester);
        bool CanEditBlog(Blogg blog, User requester);
        bool CanEditUser(User user, User requester);
        bool CanBan(User user, User requester);
        bool CanRemoveFromBan(User user, User requester);
        bool CanChangeRole(User user, User requester);
        bool CanChangeUserRole(long userid, long requesterid);
        bool CanBanUser(long userid, long requesterid);
        bool CanRemoveUserFromBan(long userid, long requesterid);
        bool CanAddComment(Post post, User requester);
    }
}
