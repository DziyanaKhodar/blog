﻿using Blog.Data.Contracts;
using Blog.Data.Models;
using System.Collections.Generic;
using System.Linq;


namespace Blog.Lib.Contracts
{
    public interface IBlogLogic
    {
        void PreservePostComments(Post post, IEnumerable<long> commentsIds);
        void AddTagsToPost(Post post, IEnumerable<(string text, long id)> tags);
        IQueryable<Post> SearchPostsByUserTagsOrWords(string userName, string[] tags, string[] words, long? blogId, long? userId);
        bool AddOrUpdateBlog(Blogg blog, long user);
        bool DeleteBlog(Blogg blog, long user);
        bool AddPost(Post post, long user);
        bool UpdatePost(Post post, long user);
        bool DeletePost(Post post, long user);
        bool DeleteComment(Comment comment, long user);
        bool UpdateUser(User user, long requester);
        bool IsExisting(IEntity entity);
        bool IsExisting(long id);
    }
}
