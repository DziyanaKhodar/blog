﻿using Blog.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Blog.Lib.Contracts
{
    public interface IAccountPermissionsManager
    {
        Task<bool> ChangeRoles(long userid, long requester, IEnumerable<string> roles);
        Task<bool> Ban(long userid, long requester);
        Task<bool> RemoveFromBan(long userid, long requester);
        Task<IEnumerable<(string role, bool member)>> GetUserRolesAsync(long userid);
    }
}