﻿using Blog.Data.Constants;
using Blog.Data.Contracts;
using Blog.Data.Models;
using Blog.Lib.Contracts;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Blog.Lib
{
    public class AccountPermissionsManager : IAccountPermissionsManager
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPermissions _permissions;
        private readonly RoleManager<Role> _roleManager;
        private readonly UserManager<User> _userManager;

        public AccountPermissionsManager
        (
            IPermissions permissions,
            RoleManager<Role> roleManager,
            UserManager<User> userManager,
            IUnitOfWork unitOfWork
        )
        {
            _unitOfWork = unitOfWork;
            _permissions = permissions;
            _roleManager = roleManager;
            _userManager = userManager;
        }

        private IEnumerable<string> GetAllRoles()
        {
            return new[] { RolesNames.Admin, RolesNames.Moderator };
        }

        public async Task<IEnumerable<(string role, bool member)>> GetUserRolesAsync(long userid)
        {
            var user = _unitOfWork.UserRepository.GetById(userid);
            var currentUserRoles = await _userManager.GetRolesAsync(user);
            var roles = GetAllRoles();
            var res = new List<(string role, bool member)>();
            foreach(var role in roles)
            {
                var tuple = (role, false);
                if(currentUserRoles.Any(x => role.Equals(x, StringComparison.OrdinalIgnoreCase)))
                {
                    tuple.Item2 = true;
                }

                res.Add(tuple);
            }
            return res;
        }

        public async Task<bool> ChangeRoles(long userid, long requesterid, IEnumerable<string> roles)
        {
            if (!_permissions.CanChangeUserRole(userid, requesterid))
            {
                return false;
            }

            var user = await _userManager.FindByIdAsync(userid.ToString());
            var currentRoles = await _userManager.GetRolesAsync(user);
            var rolesToRemove = currentRoles.Except(roles);
            var removeRes = await RemoveRoles(user, rolesToRemove);
            if(!removeRes)
            {
                return false;
            }

            return await AddRoles(user, roles);
        }

        public async Task<bool> Ban(long userid, long requesterid)
        {
            if(!_permissions.CanBanUser(userid, requesterid))
            {
                return false;
            }

            var user = await _userManager.FindByIdAsync(userid.ToString());
            var res = await _userManager.AddClaimAsync(user, new Claim(ClaimsTypes.Banned, "true"));
            return res.Succeeded;
        }

        private async Task<bool> RemoveAllRoles(User user)
        {
            var roles = await _userManager.GetRolesAsync(user);
            return await RemoveRoles(user, roles);
        }

        private async Task<bool> RemoveRoles(User user, IEnumerable<string> roles)
        {
            var res = await _userManager.RemoveFromRolesAsync(user, roles);

            return res.Succeeded;
        }

        private async Task<bool> AddRoles(User user, IEnumerable<string> newRoles)
        {
            var currentRoles = await _userManager.GetRolesAsync(user);
            newRoles = newRoles.Except(currentRoles);
            var addRes = await _userManager.AddToRolesAsync(user, newRoles);

            return addRes.Succeeded;
        }

        public async Task<bool> RemoveFromBan(long userid, long requesterid)
        {
            if (!_permissions.CanRemoveUserFromBan(userid, requesterid))
            {
                return false;
            }

            var user = await _userManager.FindByIdAsync(userid.ToString());
            var claims = await _userManager.GetClaimsAsync(user);
            var bannedClaim = claims.FirstOrDefault(x => x.Type.Equals(ClaimsTypes.Banned, StringComparison.OrdinalIgnoreCase));
            if(bannedClaim == null)
            {
                return false;
            }
            var res = await _userManager.RemoveClaimAsync(user, bannedClaim);
            return res.Succeeded;
        }

       
    }
}
