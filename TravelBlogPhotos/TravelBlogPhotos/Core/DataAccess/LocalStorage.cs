﻿using TravelBlogPhotos.Contracts;
using System.Collections.Generic;
using System.IO;
using SQLite;
using System;
using System.Linq;

namespace TravelBlogPhotos.Core.DataAccess
{
    public class LocalStorage : ILocalStorage
    {
        public Account[] GetAllAccounts()
        {
            using (var dbConnection = new SQLiteConnection(GetDbPath()))
            {
                var data = dbConnection.Table<Account>();
                return data.ToArray();
            }
        }

        public bool UserExists(string email)
        {
            return GetAllAccounts().Any(x => x.Email == email);
        }

        public bool Add(Account account)
        {
            var result = new List<Account>();
            using (var dbConnection = new SQLiteConnection(GetDbPath()))
            {
                try
                {
                    dbConnection.Insert(account);
                }
                catch(Exception ex)
                {
                    return false;
                }
                
            }

            return true;
        }

        public void Delete(long id)
        {
            var result = new List<Account>();
            using (var dbConnection = new SQLiteConnection(GetDbPath()))
            {
                var account = dbConnection.Table<Account>().Where(x => x.Id == id).First();
                dbConnection.Delete(account);                
            }
        }

        public void EnsureDatabaseCreated()
        {
            var directoryPath = Path.Combine(StorageUtility.GetExtarnalStorageDirectory(), Constants.LocalStorageDirectoryName);
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }
            var filePath = Path.Combine(directoryPath, Constants.LocalStorageFileName);
            if (!File.Exists(filePath))
            {
                CreateDatabase(filePath);
            }
        }

        private void CreateDatabase(string fileName)
        {

            using (var dbConnection = new SQLiteConnection(fileName))
            {
                dbConnection.CreateTable<Account>();
            }
        } 

        private string GetDbPath()
        {
            var directoryPath = Path.Combine(StorageUtility.GetExtarnalStorageDirectory(), Constants.LocalStorageDirectoryName);
            return Path.Combine(directoryPath, Constants.LocalStorageFileName);
        }

        public void UpdateRegistrationId(string registrationId)
        {
            var directoryPath = StorageUtility.GetExtarnalStorageDirectory();
            var filePath = Path.Combine(directoryPath, Constants.RegistrationIdFileName);

            File.WriteAllText(filePath, registrationId);
        }

        public string GetRegistrationId()
        {
            var directoryPath = StorageUtility.GetExtarnalStorageDirectory();
            var filePath = Path.Combine(directoryPath, Constants.RegistrationIdFileName);

            return File.ReadAllText(filePath).Trim();
        }
    }
}