﻿using TravelBlogPhotos.Contracts;
using TravelBlogPhotos.DataModels;
using System.Net.Http;
using System.Threading.Tasks;
using Flurl;
using Newtonsoft.Json;

namespace TravelBlogPhotos.Core
{
    public class ApiClient : IApiClient
    {
        private const string _addAccountEndPoint = "api/device";
        private const string _sendPhotoEndPoint = "post/photo";

        public bool AddAccount(
            string email,
            string password,
            DeviceRegistrationUpdateInfo deviceRegistrationUpdateInfo,
            out long? userId
        )
        {
            var httpClient = new HttpClient();
            var uri = Url.Combine(Constants.BackendAddress, _addAccountEndPoint);

            var jsonContent = JsonConvert.SerializeObject(deviceRegistrationUpdateInfo);
            var content = new StringContent(jsonContent);
            content.Headers.Remove("Content-Type");
            content.Headers.Add("Content-Type", "application/json");

            using (var request = new HttpRequestMessage(HttpMethod.Put, uri))
            {
                request.Content = content;
                request.Headers.Add("email", email);
                request.Headers.Add("password", password);

                var response = Task.Run(() => httpClient.SendAsync(request)).Result;

                if (!response.IsSuccessStatusCode)
                {
                    userId = null;
                    return false;
                }

                var strUserId = Task.Run(() => response.Content.ReadAsStringAsync()).Result;
                userId = long.Parse(strUserId);

                return true;
            }
        }

        public bool SendImage(string base64Image, int positionId, string clientId)
        {
            var image = new Image
            {
                Base64ImageContent = base64Image,
                PositionId = positionId,
                CLientId = clientId
            };

            var content = new StringContent(JsonConvert.SerializeObject(image));
            content.Headers.Remove("Content-Type");
            content.Headers.Add("Content-Type", "application/json");

            var uri = Url.Combine(Constants.BackendAddress, _sendPhotoEndPoint);
            var httpClient = new HttpClient();

            using (var request = new HttpRequestMessage(HttpMethod.Post, uri))
            {
                request.Content = content;
                var response = Task.Run(() => httpClient.SendAsync(request)).Result;

                return response.IsSuccessStatusCode;
            }
        }
    }
}