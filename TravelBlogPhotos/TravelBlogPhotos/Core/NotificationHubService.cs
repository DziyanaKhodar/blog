﻿using Firebase.Iid;
using System.IO;
using System.Linq;
using TravelBlogPhotos.Contracts;
using TravelBlogPhotos.Core.DataAccess;
using TravelBlogPhotos.DataModels;

namespace TravelBlogPhotos.Core
{
    public class NotificationHubService : INotificationHubService
    {
        public bool AddAccount(string email, string password, out long? userId)
        {
            var localStorage = new LocalStorage();
            var accounts = localStorage.GetAllAccounts().Select(x => x.Email).ToList();
            
            var registrationId = localStorage.GetRegistrationId();
            var handle = FirebaseInstanceId.Instance.Token;

            var deviceRegistrationUpdateInfo = new DeviceRegistrationUpdateInfo
            {
                Handle = handle,
                RegistrationId = registrationId,
                Accounts = accounts.ToArray()
            };

            var apiClient = new ApiClient();
            return apiClient.AddAccount(email, password, deviceRegistrationUpdateInfo, out userId);
        }      
    }
}