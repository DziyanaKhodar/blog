﻿
namespace TravelBlogPhotos.Contracts
{
    public interface ILocalStorage
    {
        bool Add(Account account);
        void Delete(long id);
        Account[] GetAllAccounts();
        void EnsureDatabaseCreated();
        void UpdateRegistrationId(string registrationId);
        string GetRegistrationId();
    }
}