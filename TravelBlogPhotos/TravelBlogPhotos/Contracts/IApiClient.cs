﻿using TravelBlogPhotos.DataModels;

namespace TravelBlogPhotos.Contracts
{
    public interface IApiClient
    {
        bool SendImage(string base64Image, int positionId, string clientId);
        bool AddAccount(string email, string password, DeviceRegistrationUpdateInfo deviceRegistrationUpdateInfo, out long? userId);
    }
}