﻿
namespace TravelBlogPhotos.Contracts
{
    public interface IAuthenticationService
    {
        bool Authenticate(string email, string password, out long? id);
    }
}