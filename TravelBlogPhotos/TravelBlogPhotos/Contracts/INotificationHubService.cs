﻿
namespace TravelBlogPhotos.Contracts
{
    public interface INotificationHubService
    {
        bool AddAccount(string email, string password, out long? userId);       
    }
}