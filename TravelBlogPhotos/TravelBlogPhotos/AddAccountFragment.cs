﻿using Android.App;
using Android.OS;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using System;
using TravelBlogPhotos.Core;
using TravelBlogPhotos.Core.DataAccess;

namespace TravelBlogPhotos
{
    public class AddAccountFragment : Fragment
    {
        private EditText _email;
        private EditText _password;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            var rootView = inflater.Inflate(Resource.Layout.add_account, container, false);
            _email = rootView.FindViewById<EditText>(Resource.Id.email);
            _password = rootView.FindViewById<EditText>(Resource.Id.password);
            var addAccountBtn = rootView.FindViewById<Button>(Resource.Id.add_account_btn);

            addAccountBtn.Click += AddAccount;

            return rootView;
        }

        private void AddAccount(object sender, EventArgs args)
        {
            var email = _email.Text?.Trim();
            var password = _password.Text?.Trim();

            if(string.IsNullOrWhiteSpace(email) || string.IsNullOrWhiteSpace(password))
            {
                Toast.MakeText(Context, Resource.String.empty_creds, ToastLength.Long).Show();
                return;
            }

            var localStorage = new LocalStorage();
            if (localStorage.UserExists(email))
            {
                Toast.MakeText(Context, Resource.String.account_already_added, ToastLength.Long).Show();

            }
            else
            {
                long? id = null;
                var notificationHubService = new NotificationHubService();
                if(!notificationHubService.AddAccount(email, password, out id))
                {
                    Toast.MakeText(Context, Resource.String.wrong_creds, ToastLength.Long).Show();
                    return;
                }
                localStorage.Add(new Account { Email = email, Id = (long)id });
                Toast.MakeText(Context, Resource.String.success, ToastLength.Long).Show();        
            }
            HideKeyboard();
        }

        public void HideKeyboard()
        {
            var imm = (InputMethodManager)Activity.GetSystemService(Activity.InputMethodService);
            int sdk = (int)Build.VERSION.SdkInt;

            if (sdk < 11)
            {
                if (Activity.Window.CurrentFocus == null)
                    return;

                imm.HideSoftInputFromWindow(Activity.Window.CurrentFocus.WindowToken, 0);
            }
            else
            {
                if (Activity.CurrentFocus == null)
                    return;

                imm.HideSoftInputFromWindow(Activity.CurrentFocus.WindowToken, HideSoftInputFlags.NotAlways);
            }
        }
    }
}