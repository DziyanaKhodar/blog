﻿using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Content;
using Android.Util;
using Firebase.Iid;
using TravelBlogPhotos.Core.DataAccess;
using WindowsAzure.Messaging;

namespace TravelBlogPhotos.NotificationsManagement
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.INSTANCE_ID_EVENT" })]
    public class TravelBlogFirebaseInstanceIdService : FirebaseInstanceIdService
    {
        private const string _tag = "MyFirebaseIIDService";
        private NotificationHub hub;

        public override void OnTokenRefresh()
        {
            var refreshedToken = FirebaseInstanceId.Instance.Token;
            Log.Debug(_tag, "FCM token: " + refreshedToken);
            SendRegistrationToServer(refreshedToken);
        }

        void SendRegistrationToServer(string token)
        {
            // Register with Notification Hubs
            hub = new NotificationHub
            (
                Constants.NotificationHubName,
                Constants.ListenConnectionString,
                this
            );

            var localStorage = new LocalStorage();
            var tags = localStorage.GetAllAccounts().Select(x => x.Email);
            var regID = hub.Register(token, tags.ToArray()).RegistrationId;
            
            localStorage.UpdateRegistrationId(regID);

            Log.Debug(_tag, $"Successful registration of ID {regID}");
        }
    }
}