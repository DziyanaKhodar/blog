﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V4.App;
using Android.Util;
using Firebase.Messaging;
using System.Linq;
using Newtonsoft.Json;
using TaskStackBuilder = Android.App.TaskStackBuilder;

namespace TravelBlogPhotos.NotificationsManagement
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.MESSAGING_EVENT" })]
    public class TravelBlogFirebaseMessagingService : FirebaseMessagingService
    {
        private const string _tag = "TravelBlogFirebaseMessagingService";
        public override void OnMessageReceived(RemoteMessage message)
        {
            Log.Debug(_tag, "From: " + message.From);
            if (message.GetNotification() != null)
            {
                //These is how most messages will be received
                Log.Debug(_tag, "Notification Message Body: " + message.GetNotification().Body);
                SendNotification(message.GetNotification().Body);
            }
            else
            {
                //Only used for debugging payloads sent from the Azure portal
                SendNotification(message.Data.Values.First());

            }
        }

        void SendNotification(string messageBody)
        {
            var message = ParseBody(messageBody);

            var resultIntent = new Intent(this, typeof(PhotoActivity));

            resultIntent.PutExtra("clientId", message.ClientId);
            resultIntent.PutExtra("positionId", message.PositionId);

            var resultPendingIntent = PendingIntent.GetActivity(this, 0, resultIntent, PendingIntentFlags.UpdateCurrent);

            var notificationBuilder = new NotificationCompat.Builder(this)
                        .SetContentTitle($"Please, choose a photo for {message.UserEmail}")
                        .SetSmallIcon(Resource.Drawable.ic_launcher)
                        .SetAutoCancel(true)
                        .SetShowWhen(false)
                        .SetContentIntent(resultPendingIntent);

            if (Build.VERSION.SdkInt >= BuildVersionCodes.O)
            {
                notificationBuilder.SetChannelId(MainActivity.NotificationsChannelId);
            }

            var notificationManager = NotificationManager.FromContext(this);

            notificationManager.Notify(0, notificationBuilder.Build());
        }
        
        private NotificationMessageModel ParseBody(string body)
        {
            return JsonConvert.DeserializeObject<NotificationMessageModel>(body);
        }
    }
}