﻿using Android;
using Android.App;
using Android.Content.PM;
using Android.Gms.Common;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Android.Support.V7.App;
using Android.Util;
using Android.Views;
using System;
using TravelBlogPhotos.Core.DataAccess;
using AlertDialog = Android.App.AlertDialog;

namespace TravelBlogPhotos
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity, BottomNavigationView.IOnNavigationItemSelectedListener
    {
        private const int _writeExternalStorage = 111;
        private const int _internet = 222;
        private const string _tag = "MainActivity";
        public const string NotificationsChannelId = "TravelBlogChannelId";

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            BottomNavigationView navigation = FindViewById<BottomNavigationView>(Resource.Id.navigation);
            navigation.SetOnNavigationItemSelectedListener(this);

            if (savedInstanceState == null)
                NavigateToAddAccount();


            var permission = ContextCompat.CheckSelfPermission(this,
                Manifest.Permission.WriteExternalStorage);
            if (permission != Permission.Granted)
            {
                RequestStoragePermission();
                //RequestInternetPermission();
            }

            IsPlayServicesAvailable();
            CreateNotificationChannel();
            var storage = new LocalStorage();
            storage.EnsureDatabaseCreated();
        }

        private void RequestStoragePermission()
        {
            ActivityCompat.RequestPermissions(
                    this,
                    new string[] { Manifest.Permission.WriteExternalStorage },
                    _writeExternalStorage);
        }

        //private void RequestInternetPermission()
        //{
        //    ActivityCompat.RequestPermissions(
        //            this,
        //            new string[] { Manifest.Permission.Internet },
        //            _internet);
        //}


        public override void OnRequestPermissionsResult
        (
            int requestCode,
            string[] permissions,
            Permission[] grantResults
        )
        {
            switch (requestCode)
            {
                case _writeExternalStorage:
                    
                    if (!(grantResults.Length > 0
                            && grantResults[0] == Permission.Granted))
                    {

                        if (ActivityCompat.ShouldShowRequestPermissionRationale(this, Manifest.Permission.WriteExternalStorage))
                        {
                            ShowRequestPermissionRationale
                            (
                                Resource.String.permission_required,
                                Resource.String.perm_req_explanation,
                                RequestStoragePermission
                            );
                        }
                        else
                        {
                            RequestStoragePermission();
                        }        
                    }
                    
                    break;
                //case _internet:
                //    if (!(grantResults.Length > 0
                //            && grantResults[0] == Permission.Granted))
                //    {

                //        if (ActivityCompat.ShouldShowRequestPermissionRationale(this, Manifest.Permission.Internet))
                //        {
                //            ShowRequestPermissionRationale
                //            (
                //                Resource.String.permission_required,
                //                Resource.String.perm_req_explanation,
                //                RequestInternetPermission
                //            );
                //            //AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
                //            //alertBuilder.SetCancelable(false);
                //            //alertBuilder.SetTitle(Resource.String.permission_required);
                //            //alertBuilder.SetMessage(Resource.String.perm_req_explanation);
                //            //alertBuilder.SetPositiveButton(Resource.String.yes, (sender, eventArgs) => RequestInternetPermission());

                //            //AlertDialog alertDialog = alertBuilder.Create();
                //            //alertDialog.Show();
                //        }
                //        else
                //        {
                //            RequestInternetPermission();
                //        }
                //    }

                //    break;

            }
        }

        private void ShowRequestPermissionRationale(int title, int message, Action requestPermissionAction)
        {
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
            alertBuilder.SetCancelable(false);
            alertBuilder.SetTitle(title);
            alertBuilder.SetMessage(message);
            alertBuilder.SetPositiveButton(Resource.String.yes, (sender, eventArgs) => requestPermissionAction());

            AlertDialog alertDialog = alertBuilder.Create();
            alertDialog.Show();
        }

        public bool OnNavigationItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.navigation_home:
                    NavigateToAccountsList();
                    return true;
                case Resource.Id.navigation_dashboard:
                    NavigateToAddAccount();
                    return true;
                default:
                    return false;

            }
        }

        private void NavigateToAccountsList()
        {
            var fragmentTransaction = FragmentManager.BeginTransaction();
            fragmentTransaction.Replace(Resource.Id.container, new AccountsListFragment());
            fragmentTransaction.AddToBackStack(null);
            fragmentTransaction.Commit();
        }

        private void NavigateToAddAccount()
        {
            var fragmentTransaction = FragmentManager.BeginTransaction();
            fragmentTransaction.Replace(Resource.Id.container, new AddAccountFragment());
            fragmentTransaction.AddToBackStack(null);
            fragmentTransaction.Commit();
        }

        public bool IsPlayServicesAvailable()
        {
            int resultCode = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this);
            if (resultCode != ConnectionResult.Success)
            {
                if (GoogleApiAvailability.Instance.IsUserResolvableError(resultCode))
                    Log.Debug(_tag, GoogleApiAvailability.Instance.GetErrorString(resultCode));
                else
                {
                    Log.Debug(_tag, "This device is not supported");
                    Finish();
                }
                return false;
            }

            Log.Debug(_tag, "Google Play Services is available.");
            return true;
        }

        private void CreateNotificationChannel()
        {
            if (Build.VERSION.SdkInt < BuildVersionCodes.O)
            {
                // Notification channels are new in API 26 (and not a part of the
                // support library). There is no need to create a notification
                // channel on older versions of Android.
                return;
            }

            var channelName = NotificationsChannelId;
            var channelDescription = string.Empty;
            var channel = new NotificationChannel(NotificationsChannelId, channelName, NotificationImportance.High)
            {
                Description = channelDescription
            };

            var notificationManager = (NotificationManager)GetSystemService(NotificationService);
            notificationManager.CreateNotificationChannel(channel);
        }
    }
}

