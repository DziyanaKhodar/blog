﻿using System;
using System.Collections.Generic;
using Android.Content;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using TravelBlogPhotos.Core.DataAccess;

namespace TravelBlogPhotos
{
    class AccountsListAdapter : RecyclerView.Adapter
    {
        private IList<Account> _accounts;

        public class ViewHolder:RecyclerView.ViewHolder
        {
            public TextView TextView { get; set; }
            public Button DeleteButton { get; set; }

            public ViewHolder(View itemView): base(itemView)
            {
                TextView = itemView.FindViewById<TextView>(Resource.Id.accountTextView);
                DeleteButton = itemView.FindViewById<Button>(Resource.Id.accountDeleteButton);
            }
        }

        public AccountsListAdapter(IList<Account> accounts)
        {
            _accounts = accounts;
        }

        private void Delete(long id, Action callBack)
        {
            var localStorage = new LocalStorage();
            localStorage.Delete(id);
            callBack();
        }

        private void OnDeleteCompleted(int position)
        {
            _accounts.RemoveAt(position);
            NotifyItemRemoved(position);
            NotifyItemRangeChanged(position, ItemCount);
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var customHolder = holder as ViewHolder;
            customHolder.TextView.Text = _accounts[position].Email;
            ((ViewHolder)holder).DeleteButton.Click += delegate (object sender, EventArgs args)
            {
                var _position = position;
                Delete(_accounts[_position].Id, () => OnDeleteCompleted(_position));
            };
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.account_layout, parent, false);
            return new ViewHolder(itemView);
        }

        public override int ItemCount
        {
            get
            {
                return _accounts.Count;
            }
        }
    }
}