﻿using System;
using System.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using TravelBlogPhotos.Core;
//using System.Drawing;
//using System.Drawing.Imaging;

namespace TravelBlogPhotos
{
    [Activity(Label = "PhotoActivity")]
    public class PhotoActivity : Activity
    {
        private int _selectPictureId = 666;
        private string _clientId;
        private int _positionId;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            _clientId = Intent.GetStringExtra("clientId");
            _positionId = Intent.GetIntExtra("positionId", 0);
            StartGallery();

            // Create your application here
        }

        private void StartGallery()
        {
            var intent = new Intent();
            intent.SetType("image/*");
            intent.SetAction(Intent.ActionGetContent);
            StartActivityForResult(Intent.CreateChooser(intent, "Select Picture"), _selectPictureId);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            if ((requestCode == _selectPictureId) && (resultCode == Result.Ok) && (data != null))
            {
                var uri = data.Data;
                var bytesImage = ConvertImageToBytes(uri);
                var base64Image = Convert.ToBase64String(bytesImage);

                if (!SendPicture(base64Image))
                {
                    Toast.MakeText(this, Resource.String.wrong_creds, ToastLength.Long);
                }
                else
                {
                    Finish();
                }

            }
        }

        private byte[] ConvertImageToBytes(Android.Net.Uri uri)
        {
            Stream stream = ContentResolver.OpenInputStream(uri);
            //Bitmap b = (Bitmap)Bitmap.FromStream(stream);

            byte[] byteArray;

            using (var memoryStream = new MemoryStream())
            {
                // b.Save(memoryStream, ImageFormat.Png);
                stream.CopyTo(memoryStream);
                byteArray = memoryStream.ToArray();
            }
            return byteArray;
        }

        private bool SendPicture(string base64Image)
        {
            var apiClient = new ApiClient();
            return apiClient.SendImage(base64Image, _positionId, _clientId);
        }
    }
}