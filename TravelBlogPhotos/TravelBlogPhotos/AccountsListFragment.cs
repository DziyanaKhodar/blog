﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using TravelBlogPhotos.Core.DataAccess;

namespace TravelBlogPhotos
{
    public class AccountsListFragment : Fragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var rootView = inflater.Inflate(Resource.Layout.accounts_list_fragment_layout, container, false);

            var layoutManager = new LinearLayoutManager(Context);

            var localStorage = new LocalStorage();
            var accounts = localStorage.GetAllAccounts();//new[] { new Account { Email = "test", Id = 1 }, new Account { Email = "test2", Id = 2 } };

            var recyclerView = rootView.FindViewById<RecyclerView>(Resource.Id.accounts_list);
            recyclerView.SetLayoutManager(layoutManager);
            recyclerView.SetAdapter(new AccountsListAdapter(accounts.ToList()));

            return rootView;
        }
    }
}