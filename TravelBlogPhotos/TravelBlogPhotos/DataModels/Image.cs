﻿
namespace TravelBlogPhotos.DataModels
{
    public class Image
    {
        public string Base64ImageContent { get; set; }
        public string CLientId { get; set; }
        public int PositionId { get; set; }
    }
}