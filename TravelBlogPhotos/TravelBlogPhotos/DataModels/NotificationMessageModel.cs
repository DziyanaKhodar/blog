﻿
namespace TravelBlogPhotos
{
    public class NotificationMessageModel
    {
        public string ClientId { get; set; }
        public int PositionId { get; set; }
        public string UserEmail { get; set; }
    }
}