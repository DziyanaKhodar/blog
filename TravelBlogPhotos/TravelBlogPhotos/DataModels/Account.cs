﻿
using SQLite;

namespace TravelBlogPhotos
{
    public class Account
    {
        [MaxLength(100)]
        public string Email { get; set; }

        [PrimaryKey]
        public long Id { get; set; }
    }
}