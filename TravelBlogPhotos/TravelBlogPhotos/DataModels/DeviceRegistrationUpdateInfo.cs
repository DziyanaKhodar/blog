﻿
namespace TravelBlogPhotos.DataModels
{
    public class DeviceRegistrationUpdateInfo
    {
        public string Handle { get; set; }
        public string[] Accounts { get; set; }
        public string RegistrationId { get; set; }
    }
}